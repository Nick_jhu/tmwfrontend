var APIBaseURL
var APIBaseURLPort
var localAPIURL
var localAPIURLPort
var EDIHubURL
var EDIHubURLPort
var downloadEDIHubURL
var downloadAPIBaseURL
//
// console.log('NODE_ENV:', process.env.NODE_ENV)
//
switch (process.env.NODE_ENV) {
  case 'production':
    // 正式上線
    APIBaseURL = 'https://slctms.standard-info.com'
    APIBaseURLPort = '17890'
    EDIHubURL = 'https://slctms.standard-info.com'
    EDIHubURLPort = '16892'
    downloadEDIHubURL = 'https://slctms.standard-info.com'
    localAPIURL = `https://slctms.standard-info.com`
    localAPIURLPort = ':17880'
    downloadAPIBaseURL = 'https://slctms.standard-info.com'
    break
  case 'development':
  default:
    // 開發階段
    APIBaseURL = 'https://slctms.standard-info.com'
    APIBaseURLPort = '17890'
    EDIHubURL = 'https://aws2-wms.target-ai.com'
    EDIHubURLPort = '16892'
    downloadEDIHubURL = 'https://aws2-wms.target-ai.com'
    localAPIURL = `http://localhost`
    localAPIURLPort = ':8082'
    downloadAPIBaseURL = 'https://aws2-wms.target-ai.com'
    break
}
//
const config = {
  version: '0.0.1',
  firstPath: '/Main',
  maxPageSize: 1000,
  langList: [
    {
      value: 'zh_TW',
      label: '繁體中文'
    },
    {
      value: 'zh_CN',
      label: '简体中文'
    },
    {
      value: 'en_US',
      label: 'English'
    }
  ],
  themeColor: {
    dark: {
      header: '#3c3c3c',
      footer: '#535353',
      menu: '#118cf8',
      menuSetUser: '#3c3c3c',
      menuColor: '#ffffff',
      rowMenuColor: '#3c4b69',
      menuColorActive: '#FACC45',
      menuActive: '#354361',
      mainContainer: '#3c3c3c',
      windowHeader: '#E0E0E0',
      windowHeaderFontColor: '#FFFFFF',
      windowHeaderIconColor: '#888888',
      windowFontColor: '#FFFFFF',
      windowSubFontColor: '#4eedc5',
      dataGrid: {
        headBgColor: '#118cf8',
        headColor: '#fff',
        searchBarBgColor: '#605E58',
        rowOddBgColor: '#3c3c3c',
        rowOddColor: '#fff',
        rowEvenBgColor: '#3c3c3c',
        rowEvenColor: '#fff',
        selectedColor: '#ffd0dd'
      },
      btn: {
        add: {
          background: '#4eedc5',
          border: '#fff',
          color: '#fff'
        },
        remove: {
          background: '#ff3875',
          border: '#ff3875',
          color: '#fff'
        },
        upload: {
          background: '#118cf8',
          border: '#118cf8',
          color: '#fff'
        },
        reload: {
          background: '#dca558',
          border: '#dca558',
          color: '#fff'
        }
      }
    },
    light: {
      header: '#ffffff',
      footer: '#535353',
      menu: '#3c3c3c',
      menuSetUser: '#ECEFF2',
      menuColor: '#777777',
      rowMenuColor: '#777777',
      menuColorActive: '#36aabd',
      menuActive: '#fde0e0',
      mainContainer: '#fff',
      windowHeader: '#fde0e0',
      windowHeaderFontColor: '#777777',
      windowHeaderIconColor: '#C3BFC6',
      windowFontColor: '#3c3c3c',
      windowSubFontColor: '#118cf8',
      dataGrid: {
        headBgColor: '#118cf8',
        headColor: '#fff',
        searchBarBgColor: '#ECEFF2',
        rowOddBgColor: '#fff',
        rowOddColor: '#3c3c3c',
        rowEvenBgColor: '#fff',
        rowEvenColor: '#3c3c3c',
        selectedColor: '#ffd0dd'
      },
      btn: {
        add: {
          background: '#4eedc5',
          border: '#fff',
          color: '#fff'
        },
        remove: {
          background: '#ff3875',
          border: '#ff3875',
          color: '#fff'
        },
        upload: {
          background: '#118cf8',
          border: '#118cf8',
          color: '#fff'
        },
        reload: {
          background: '#dca558',
          border: '#dca558',
          color: '#fff'
        }
      }
    }
  },
  logicalOperator: [
    {
      value: '=',
      label: 'equal'
    },
    {
      value: 'like',
      label: 'like'
    },
    {
      value: '!=',
      label: 'not like'
    },
    {
      value: '>=',
      label: '>='
    },
    {
      value: '<=',
      label: '<='
    }
  ],
  dataGridPageSize: 50,
  API: {
    baseURL: APIBaseURL,
    port: APIBaseURLPort,
    EDIHubURL: EDIHubURL,
    EDIHubURLPort: EDIHubURLPort,
    downloadEDIHubURL: downloadEDIHubURL,
    rootPath: '/api',
    localAPIURL: localAPIURL,
    localAPIURLPort: localAPIURLPort,
    localRootPath: '/API',
    downloadAPIBaseURL: downloadAPIBaseURL,
    Auth: {
      login: '/login', // 登入
      logout: '/logout', // 登出
      register: '/register.json', // 註冊
      verifyEmail: '/verifyEmail.json',
      verifyToken: '/check/token', // 驗證Token
      refreshtoken: '/refreshtoken', // 重取Token
      sendVerifyEmail: '/sendVerifyEmail.json', // 發驗證信
      resendVerifyEmail: '/resendVerifyEmail.json', // 重發驗證信
      resetPWD: '/resetPWD.json', // 重設密碼
      setUserPreference: '', // 設定使用者偏好
      delUserPreference: '', // 刪除使用者偏好
      userPreference: '', // 取得使用者偏好
      maxLoginUser: '/config.json', // 取得線上用戶數量限制
      onlineuserQuery: '/get/online/user', // 取得目前線上用戶清單
      onlineuserLogout: '/superLogout' // 強制登出某個線上未登出帳號
    },
    companyGroup: {
      update: '/company-group/', // PUT
      query: '/company-group/q', // POST
      delete: '/company-group/', // DELETE
      create: '/company-group/a' // POST
    },
    company: {
      update: '/company/', // PUT
      query: '/company/q', // POST
      delete: '/company/', // DELETE
      create: '/company/a', // POST
      layoutSettings: '/company/layout-settings/' // PUT
    },
    department: {
      update: '/department/', // PUT
      query: '/department/q', // POST
      delete: '/department/', // DELETE
      create: '/department/a' // POST
    },
    warehouse: {
      update: '/warehouse/', // PUT
      query: '/warehouse/query', // POST
      queryDetail: '/warehouse/', // GET
      delete: '/warehouse/batch/delete', // DELETE
      create: '/warehouse', // POST
      dataexport: '/warehouse/excel/export', // 匯出
      import: '/warehouse/excel/import' // 匯入
    },
    country: {
      update: '/country/', // PUT
      query: '/country/q', // POST
      delete: '/country/', // DELETE
      create: '/country/a' // POST
    },
    stateProvince: {
      update: '/state-province/', // PUT
      query: '/state-province/q', // POST
      delete: '/state-province/', // DELETE
      create: '/state-province/a' // POST
    },
    city: {
      update: '/city/', // PUT
      query: '/city/q', // POST
      delete: '/city/', // DELETE
      create: '/city/a' // POST
    },
    district: {
      update: '/district/', // PUT
      query: '/district/q', // POST
      delete: '/district/', // DELETE
      create: '/district/a' // POST
    },
    product: {
      update: '/product/', // PUT
      query: '/product/query', // POST
      queryDetail: '/product/', // GET
      delete: '/product/batch/delete', // DELETE
      deleteProductRelations: '/productrelation/batch/delete', // 組合商品 DELETE
      deleteUnitConvert: '/unitconvertImport/batch/delete', // 單位換算 DELETE
      create: '/product', // POST
      dataexport: '/product/excel/export', // 商品匯出
      import: '/product/excel/import' // 匯入
    },
    noVirtualProduct: {
      query: '/product/no/virtual/product/query' // POST
    },
    withoutInProductRelationParent: {
      query: '/product/without_in_product_relation_parent_product_id/' // POST
    },
    productCategory: {
      update: '/product/category/', // PUT
      query: '/product/category/', // POST
      delete: '/product/category/', // DELETE
      create: '/product/category/a' // POST
    },
    customerCategory: {
      update: '/customer/category/', // PUT
      query: '/customer/category/q', // POST
      delete: '/customer/category/', // DELETE
      create: '/customer/category/a' // POST
    },
    order: {
      update: '/order/', // PUT
      query: '/order/query', // POST
      queryDetail: '/order/', // GET
      delete: '/order/batch/delete', // DELETE
      create: '/order', // POST
      dataexport: '/order/excel/export', // 匯出
      orderCountStatus: '/order/count/status', // 查詢各狀態總量
      import: '/order/excel/import' // 匯入
    },
    dlvplan: {
      update: '/order/', // PUT
      query: '/order/query', // POST
      queryDetail: '/order/', // GET
      delete: '/order/batch/delete', // DELETE
      create: '/order', // POST
      dataexport: '/order/excel/export', // 匯出
      orderCountStatus: '/order/count/status' // 查詢各狀態總量
    },
    dlvmission: {
      update: '/dlvmission/', // PUT
      query: '/dlvmission/query', // POST
      queryDetail: '/dlvmission/', // GET
      delete: '/dlvmission/batch/delete', // DELETE
      create: '/dlvmission', // POST
      dataexport: '/dlvmission/excel/export', // 匯出
      orderCountStatus: '/dlvmission/count/status' // 查詢各狀態總量
    },
    basicParameter: {
      update: '/bscode/', // PUT
      query: '/bscode/query', // POST
      queryDetail: '/bscode/', // GET
      delete: '/bscode/batch/delete', // DELETE
      create: '/bscode', // POST
      dataexport: '/bscode/excel/export', // 匯出
      import: '/bscode/excel/import' // 匯入
    },
    customer: {
      update: '/customer/', // PUT
      query: '/customer/query', // POST
      queryDetail: '/customer/', // GET
      delete: '/customer/batch/delete', // DELETE
      create: '/customer', // POST
      dataexport: '/customer/excel/export', // 匯出
      import: '/customer/excel/import' // 匯入
    },
    customerOwner: {
      query: '/customer/owner/query' // POST
    },
    customerStore: {
      query: '/customer/store/query' // POST
    },
    customerByCmp: {
      update: '/customer/', // PUT
      query: '/customer/', // POST
      delete: '/customer/', // DELETE
      create: '/customer/a' // POST
    },
    customerByCmpWh: {
      update: '/customer/', // PUT
      query: '/customer/', // POST
      delete: '/customer/', // DELETE
      create: '/customer/a' // POST
    },
    customerByCmpWhCat: {
      update: '/customer/', // PUT
      query: '/customer/', // POST
      delete: '/customer/', // DELETE
      create: '/customer/a' // POST
    },
    customerByCmpCat: {
      update: '/customer/', // PUT
      query: '/customer/', // POST
      delete: '/customer/', // DELETE
      create: '/customer/a' // POST
    },
    code: {
      update: '/code/', // PUT
      query: '/code/q', // POST
      delete: '/code/', // DELETE
      create: '/code/a' // POST
    },
    settingCode: {
      update: '/code/', // PUT
      query: '/setting/code/query', // POST
      delete: '/code/', // DELETE
      create: '/code/a' // POST
    },
    settingCodeByCompanyCodeType: {
      update: '/code/', // PUT
      query: '/code/', // POST
      delete: '/code/', // DELETE
      create: '/code/a' // POST
    },
    unit: {
      update: '/unit/', // PUT
      query: '/unit/q', // POST
      delete: '/unit/', // DELETE
      create: '/unit/a' // POST
    },
    settingUnit: {
      update: '/unit/', // PUT
      query: '/unit/q', // POST
      delete: '/unit/', // DELETE
      create: '/unit/a' // POST
    },
    contact: {
      update: '/contact/', // PUT
      query: '/contact/', // POST
      delete: '/contact/', // DELETE
      create: '/contact/a', // POST
      upload: '/fileupload/dataimport', // 資料上傳
      import: '/dataimport/contact', // 匯入
      dataexport: '/dataexport/contact'
    },
    user: {
      update: '/user/', // PUT
      query: '/user/query', // POST
      queryDetail: '/user/', // GET
      delete: '/user/batch/delete', // DELETE
      create: '/user' // POST
    },
    custuser: {
      update: '/user/', // PUT
      query: '/custuser/query', // POST
      queryDetail: '/user/', // GET
      delete: '/user/batch/delete', // DELETE
      create: '/user' // POST
    },
    car: {
      update: '/car/', // PUT
      query: '/car/query', // POST
      queryDetail: '/car/', // GET
      delete: '/car/batch/delete', // DELETE
      create: '/car', // POST
      dataexport: '/car/excel/export' // 匯出
    },
    userUi: {
      update: '/user/ui/', // PUT
      query: '/user/ui/q', // POST
      delete: '/user/ui/', // DELETE
      create: '/user/ui/a', // POST
      upload: '/fileupload/avator' // POST
    },
    account: {
      update: '/account/', // PUT
      query: '/account/q', // POST
      delete: '/account/', // DELETE
      create: '/account/a', // POST
      nonce: '/account/g' // POST
    },
    region: {
      update: '/region/', // PUT
      query: '/region/q', // POST
      delete: '/region/', // DELETE
      create: '/region/a' // POST
    },
    storage: {
      update: '/storage/', // PUT
      query: '/whStorage/query', // POST
      queryDetail: '/whStorage/', // GET
      delete: '/whStorage/batch/delete', // DELETE
      create: '/whStorage', // POST
      dataexport: '/whStorage/excel/export', // 匯出
      import: '/whStorage/excel/import' // 匯入
    },
    storageDetail: {
      update: '/storage/detail/', // PUT
      query: '/storage/detail/q', // POST
      delete: '/storage/detail/', // DELETE
      create: '/storage/detail/a' // POST
    },
    storageArea: {
      update: '/storage/area/', // PUT
      query: '/storage/area/q', // POST
      delete: '/storage/area/', // DELETE
      create: '/storage/area/a' // POST
    },
    companyUser: {
      update: '/company/user/', // PUT
      query: '/company/user/q', // POST
      delete: '/company/', // DELETE
      create: '/company/user/a' // POST
    },
    warehouseUser: {
      update: '/warehouse/user/', // PUT
      query: '/warehouse/user/q', // POST
      delete: '/warehouse/', // DELETE
      create: '/warehouse/user/a' // POST
    },
    inbound: {
      update: '/inbound/', // PUT
      query: '/inbound/', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    putInOperationSimple: {
      update: '/inbound/', // post
      query: '/inbound/query', // POST
      returnQuery: '/inbound/return/query', // POST
      detailQuery: '/inbound/detail/query', // POST
      queryDetail: '/inbound/and/detail', // POST
      delete: '/inbound/batch/delete', // POST
      create: '/inbound', // POST
      dataexport: '/inbound/excel/export', // 入庫單 - 匯出 Excel, CSV
      returnDataexport: '/inbound/excel/return/export', // 入庫單 - 匯出 Excel, CSV
      dataexportSnapshot: '/dataexport/inbound/snapshot/q', // 入庫歷史資料 - 匯出 Excel, CSV
      import: '/inbound/excel/import', // 匯入
      productsnImport: '/dataimport/productsn/inbound', // 匯入 - 商品序號
      inboundCountStatus: '/inbound/count/status', // 查詢各狀態總量
      inboundReturnCountStatus: '/inbound/count/return/status', // 查詢退貨各狀態總量
      accept: '/inbound/batch/confirm/accept', // 批次驗收
      qualityInspection: '/inbound/batch/confirm/quality/inspection', // 批次品檢
      putaway: '/inbound/batch/confirm/putaway', // 批次上架
      backAccept: '/inbound/batch/back/accept', // 回復驗收
      backQualityInspection: '/inbound/batch/back/quality/inspection' // 回復品檢
    },
    downloadSample: {
      download: '/download/sample' // 下載excel sample
    },
    putInOperation: {
      update: '/inbound/', // PUT
      query: '/inbound/', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a', // POST
      dataexport: '/dataexport/inbound' // 入庫單 - 匯出 Excel, CSV
    },
    putInQuality: {
      update: '/inbound/', // PUT
      query: '/inbound/qualitycheck/q', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    putInShelves: {
      update: '/inbound/', // PUT
      query: '/inbound/putaway/q', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    inboundSnapshot: {
      update: '/inbound/', // PUT
      query: '/inbound/snapshot/q', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    returnOperationSimple: {
      update: '/inbound/', // PUT
      query: '/inbound/', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a', // POST
      dataexport: '/dataexport/inbound', // 入庫單 - 匯出 Excel, CSV
      upload: '/fileupload/dataimport', // 資料上傳
      productsnImport: '/dataimport/productsn/inbound' // 匯入 - 商品序號
    },
    returnOperation: {
      update: '/inbound/', // PUT
      query: '/inbound/', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a', // POST
      dataexport: '/dataexport/inbound' // 入庫單 - 匯出 Excel, CSV
    },
    returnAcceptance: {
      update: '/inbound/', // PUT
      query: '/inbound/qualitycheck/q', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    returnShelves: {
      update: '/inbound/', // PUT
      query: '/inbound/putaway/q', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    returnSnapshot: {
      update: '/inbound/', // PUT
      query: '/inbound/snapshot/q', // POST
      delete: '/inbound/', // DELETE
      create: '/inbound/a' // POST
    },
    inboundDetail: {
      update: '/inbound/detail/', // PUT
      query: '/inbound/detail/q', // POST
      queryByCompanyWh: '/inbound/detail/', // POST
      delete: '/inbound/detail/batch/delete', // DELETE
      create: '/inbound/detail/a', // POST
      convert: '/inbound/detail/inventory/convert' // inboundDetail轉為inventory
    },
    inventory: {
      update: '/inventory/', // PUT
      query: '/inventory/', // POST
      queryByOwner: '/inventory/', // POST
      queryByCompanyWh: '/inventory/', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a', // POST
      dataexport: '/inbound/detail/excel/export'
    },
    inventoryAndOutboundDetailQuery: {
      query: '/transaction/query', // POST
      dataexport: '/transaction/excel/export' // 匯出
    },
    putInOperationSimpleInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    putInQualityInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    putInShelvesInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    inventoryQuery: {
      update: '/inventory/', // PUT
      query: '/inbound/detail/recinbound/', // POST
      queryMoreThanMonth: '/inbound/detail/recinbound/history/', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a', // POST
      dataexport: '/inbound/detail/excel/export'
    },
    inboundDetailPutaway: {
      query: '/inbound/detail/putaway/query', // POST
      dataexport: '/inbound/detail/putaway/excel/export'
    },
    returnOperationSimpleInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    returnOperationInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    returnAcceptanceInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    returnShelvesInventory: {
      update: '/inventory/', // PUT
      query: '/inventory/q', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a' // POST
    },
    returnInventoryQuery: {
      update: '/inventory/', // PUT
      query: '/inbound/detail/recinbound/', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a', // POST
      dataexport: '/dataexport/inbounddetail/recinbound/'
    },
    custom: {
      update: '/custom/', // PUT
      query: '/custom/q', // POST
      delete: '/custom/', // DELETE
      create: '/custom/a' // POST
    },
    currencyCode: {
      update: '/currency/', // PUT
      query: '/currency/q', // POST
      delete: '/currency/', // DELETE
      create: '/currency/a' // POST
    },
    outboundOperation: {
      update: '/outbound/', // PUT
      query: '/outbound/query', // POST
      queryDetail: '/outbound/and/detail', // POST
      delete: '/outbound/batch/delete', // DELETE
      create: '/outbound', // POST
      booking: '/outbound/booking', // 截單
      sendtoprint: '/outbound/sendtoprint', // 揀貨
      recovery: '/outbound/recover/outbound/status', // 回復
      recoveryBooking: '/outbound/recover/booking', // 回復截單
      confirm: '/outbound/batch/confirm', // 出貨
      batchUpdateStatus: '/outbound/batch/update/status', // 大量狀態更新
      dataexport: '/outbound/excel/export', // 出庫匯出
      outofstockDataexport: '/outbound/outofstock/record/excel/export', // 截單缺貨量匯出
      dataexportSnapshot: '/dataexport/outbound/snapshot/q', // 出庫歷史資料 - 匯出 Excel, CSV
      import: '/outbound/excel/import', // 匯入
      productsnImport: '/dataimport/productsn/outbound', // 匯入 - 商品序號
      outboundCountStatus: '/outbound/count/status',
      outboundQc: '/outbound/qc/get', // 查詢出庫品保
      outboundQualityCheck: '/outbound/quality/check', // 出庫品保
      printQualityCheck: '/outbound/print/quality/check', // 出庫品保產生揀貨單
      orderDetailBatchDeleteByProduct: '/outbound/order/detail/batch/delete/by/product', // 刪除品保明細
      outboundToInbound: '/outbound/to/inbound' // 出庫轉入庫
    },
    outboundQueryByCompanyWh: {
      query: '/outbound/' // POST
    },
    fastBoxing: {
      update: '/outbound/', // PUT
      query: '/outbound/q', // POST
      delete: '/outbound/', // DELETE
      create: '/outbound/a', // POST
      queryClose: '/outbound/close/q',
      createClose: '/outbound/close/a',
      recoveryClose: '/outbound/close/recovery'
    },
    pickingOperationOutbound: {
      update: '/outbound/', // PUT
      query: '/outbound/picking/q', // POST
      delete: '/outbound/', // DELETE
      create: '/outbound/a' // POST
    },
    outboundDetail: {
      update: '/outbound/detail/', // PUT
      query: '/outbound/detail/q', // POST
      delete: '/outbound/detail/', // DELETE
      create: '/outbound/detail/a', // POST
      dataexport: '/dataexport/outbound/detail/'
    },
    orderDetail: {
      update: '/outbound/orderdetail/', // PUT
      query: '/outbound/orderdetail/q', // POST
      queryByCompanyWh: '/outbound/orderdetail/', // POST
      delete: '/outbound/order/detail/batch/delete', // DELETE
      create: '/outbound/orderdetail/a' // POST
    },
    outboundDetailSnapshot: {
      update: '/outbound/detail/snapshot', // PUT
      query: '/outbound/detail/snapshot/q', // POST
      delete: '/outbound/detail/snapshot/', // DELETE
      create: '/outbound/detail/snapshot/a' // POST
    },
    pickingConfirmOutbound: {
      update: '/outbound/', // PUT
      query: '/outbound/pickingconfirm/q', // POST
      delete: '/outbound/', // DELETE
      create: '/outbound/pickingconfirm/a' // POST
    },
    outboundDetailQuery: {
      query: '/outbound/detail/query', // POST
      queryMoreThanMonth: '/outbound/detail/history/', // POST
      dataexport: '/outbound/detail/excel/export' // 匯出
    },
    outboundSnapshot: {
      update: '/outbound/', // PUT
      query: '/outbound/snapshot/q', // POST
      delete: '/outbound/', // DELETE
      create: '/outbound/a', // POST
      queryClose: '/outbound/close/q',
      createClose: '/outbound/close/a',
      recoveryClose: '/outbound/close/recovery'
    },
    deliveryConfirm: {
      update: '/outbound/', // PUT
      query: '/outbound/shipping/q', // POST
      delete: '/outbound/', // DELETE
      create: '/outbound/a', // POST
      deliveryConfirmAutoBoxing: '/common/system/maintain/outbound/rec_box/auto/a' // 確認出貨自動裝箱
    },
    pickinglist: {
      product: '/pickinglist/product', // 彙總揀貨單
      order: '/pickinglist/order', // 訂單揀貨單
      consignee: '/pickinglist/consignee' // 分貨單
    },
    shippinglist: {
      generate: '/shippinglist/g', // 出貨明細表
      summary: '/shippinglist/summary', // 宅配出貨總表
      hctWaybill: '/shipping/hct/waybill/confirm/outbound/', // 新竹託運總表
      tongyingWaybill: '/shipping/transportsummary/tongying/print' // 通盈託運總表
    },
    stockProduct: {
      update: '/stock/product/', // PUT
      query: '/stock/product/', // POST
      delete: '/stock/product/', // DELETE
      create: '/stock/product/a' // POST
    },
    stockStorage: {
      update: '/stock/storage/', // PUT
      query: '/stock/storage/', // POST
      delete: '/stock/storage/', // DELETE
      create: '/stock/storage/a' // POST
    },
    stockStorageBydate: {
      update: '/stock/storage/bydate/', // PUT
      query: '/stock/bydate/', // POST
      delete: '/stock/storage/bydate/', // DELETE
      create: '/stock/storage/bydate/a', // POST
      dataexport: '/dataexport/stock/bydate' // 庫存查詢 by 儲位編號 + 入庫日 - 匯出 Excel, CSV
    },
    stockStorageBySummary: {
      query: '/stock/summary', // POST
      dataexport: '/stock/summary/excel/export' // 匯出
    },
    stockStorageByAccurate: {
      update: '/stock/accurate/bydate/byinventory/', // PUT
      query: '/stock/accurate/v2/', // POST
      delete: '/stock/accurate/bydate/byinventory/', // DELETE
      create: '/stock/accurate/bydate/byinventory/a', // POST
      dataexport: '/dataexport/stock/accurate/v2/' // 匯出 Excel, CSV
    },
    importPutIn: {
      upload: '/fileupload/dataimport', // 入庫資料上傳
      import: '/dataimport/inbound/detail', // 入庫資料匯入
      importReturn: '/dataimport/inbound/detail/return'
    },
    importOutbound: {
      upload: '/fileupload/dataimport', // 出庫資料上傳
      import: '/dataimport/outbound/orderdetail' // 出庫資料匯入
    },
    importOrder: {
      upload: '/fileupload/dataimport', // 資料整合資料上傳
      import: '/dataimport/order' // 資料整合資料匯入
    },
    importCustomer: {
      upload: '/fileupload/dataimport', // 客戶資料上傳
      import: '/dataimport/customer' // 客戶資料匯入
    },
    importGoods: {
      upload: '/fileupload/dataimport', // 商品資料上傳
      import: '/dataimport/product', // 商品資料匯入
      productRelationImport: '/dataimport/product/relation', // 組合商品資料匯入
      importUnitconvert: '/dataimport/unitconvert' // 單位轉換資料匯入
    },
    importReturn: {
      upload: '/fileupload/dataimport', // 退貨入庫資料上傳
      import: '/dataimport/inbound/detail/return' // 退貨入庫資料匯入
    },
    pdfmerge: {
      pdfmerge: '/util/pdfmerge' // 產生合併 PDF file
    },
    unitConvert: {
      update: '/unit-convert/', // PUT
      query: '/unit-convert/q', // POST
      delete: '/unit-convert/', // DELETE
      create: '/unit-convert/a' // POST
    },
    transfer: {
      update: '/transfer/', // PUT
      query: '/transfer/query', // POST
      queryDetail: '/transfer/and/detail', // POST
      delete: '/transfer/batch/delete', // DELETE
      deleteDetail: '/transfer/detail/batch/delete', // DELETE
      create: '/transfer', // POST
      confirm: '/transfer/confirm', // 確認調撥
      dataexport: '/transfer/excel/export'
    },
    simpleProcessing: {
      query: '/inventory/transfer/', // POST
      create: '/inventory/transfer/a' // POST
    },
    quantityinspection: {
      quantityinspection: '/inbound/quantityinspection/report/' // 產生「進倉驗收」通知單
    },
    storageChange: {
      update: '/inventory/putaway/', // PUT
      query: '/storage/change/query', // POST
      delete: '/inventory/putaway/', // DELETE
      create: '/storage/change', // POST
      dataexport: '/storage/change/excel/export'
    },
    inboundAdjustment: {
      update: '/inbound/', // post
      query: '/inbound/query', // POST
      queryDetail: '/inbound/and/detail', // POST
      delete: '/inbound/batch/delete', // POST
      create: '/inbound' // POST
    },
    outboundAdjustment: {
      update: '/outbound/', // PUT
      query: '/outbound/query', // POST
      queryDetail: '/outbound/and/detail', // POST
      delete: '/outbound/batch/delete', // POST
      create: '/outbound', // POST
      confirm: '/adj/stock/out/confirm' // 調整出確認
    },
    inventoryQueryFiftyByOwner: {
      update: '/inventory/', // PUT
      query: '/inventory/fortransfer/byowner/', // POST
      delete: '/inventory/', // DELETE
      create: '/inventory/a', // POST
      dataexport: '/dataexport/inventory/inbound/'
    },
    productQueryByOwner: {
      update: '/product/', // PUT
      query: '/product/', // POST
      delete: '/product/', // DELETE
      create: '/product/a' // POST
    },
    boxingSetup: {
      getOutboundNoId: '/outbound/q',
      getOutboundNoIdByCompanyWh: '/outbound/',
      query: '/boxing/boxed/',
      addBoxing: '/boxing/a', // 裝箱建檔
      updatBoxing: '/boxing/', // 裝箱更新
      addBoxingDetail: '/boxing/detail/a', // 裝箱明細建檔
      queryBoxing: '/boxing/q', // 裝箱查詢
      deleteBoxing: '/boxing/', // 刪除裝箱
      queryBoxingDetail: '/boxing/detail/q', // 裝箱明細查詢
      deleteBoxingDetail: '/boxing/detail/', // 裝箱明細查詢
      updateBoxingDetail: '/boxing/detail/', // 裝箱明細更新
      comfirmBoxing: '/outbound/', // 確認裝箱
      queryPickinglist: '/pickinglist/order/', // 揀貨單明細查詢
      boxingReport: '/boxing/report/', // 產生裝箱報表
      queryProductSn: '/product/sn/boxed_nums/q', // 裝箱序號數量查詢
      checkProductSn: '/product/sn/boxed_nums/pickinglist/check' // 總出貨數量與已裝箱商品序號檢核
    },
    externalStockProduct: {
      update: '/stock/byproduct/', // PUT
      query: '/stock/byproduct/', // POST
      delete: '/stock/byproduct/', // DELETE
      create: '/stock/byproduct/a' // POST
    },
    stockByProduct: {
      query: '/stock/by/product/query', // POST
      dataexport: '/stock/by/product/excel/export' // 庫存查詢 by 商品編號 - 匯出 Excel, CSV
    },
    stockByStorage: {
      query: '/stock/by/storage/query', // POST
      dataexport: '/stock/by/storage/excel/export', // 庫存查詢 by 儲位編號 - 匯出 Excel, CSV
      byDateQuery: '/stock/by/storage/and/inbound/date', // 庫存查詢 by 儲位編號 - 匯出 Excel, CSV
      byDateDataexport: '/stock/by/storage/and/inbound/date/export' // 庫存查詢 by 儲位編號 - 匯出 Excel, CSV
    },
    storageQueryByWhId: {
      update: '/storage/', // PUT
      query: '/storage/', // POST
      delete: '/storage/', // DELETE
      create: '/storage/a' // POST
    },
    pickingListQuery: {
      query: '/pickinglist/order/q', // POST
      dataexport: '/dataexport/pickinglist/order' // 揀貨儲位查詢 - 匯出 Excel, CSV
    },
    productSn: {
      update: '/product/sn/', // PUT
      query: '/product/sn/', // POST
      delete: '/product/sn/', // DELETE
      deleteBulk: '/product/sn/bulk', // DELETE
      create: '/product/sn/a', // POST
      createBulk: '/product/sn/bulk/a', // POST
      dataexport: '/dataexport/product/sn', // 匯出 Excel, CSV
      check: '/product/sn/exist/' // POST
    },
    hctWaybill: {
      query: '/shipping/hct/waybill/outbound/', // POST
      create: '/shipping/hct/waybill/a' // POST
    },
    kerrytjWaybill: {
      create: '/shipping/ktj/waybill/a' // POST
    },
    tongyingWaybill: {
      create: '/shipping/tongying/waybill/a' // POST
    },
    printwaybill: {
      hct: '/printwaybill/hct', // POST
      kerrytj: '/printwaybill/kerrytj', // POST
      pelican: '/printwaybill/pelican', // POST
      tongying: '/printwaybill/tongying' // POST
    },
    inventorySnapshot: {
      query: '/inventory/snapshot/q', // POST
      dataexport: '/dataexport/inventory/inbound/'
    },
    inventoryBooking: {
      create: '/inventory/booking/a' // POST
    },
    boxDetailsQuery: {
      query: '/boxing/detail/q', // POST
      dataexport: '/dataexport/boxing/detail/q' // 庫存查詢 by 儲位編號 - 匯出 Excel, CSV
    },
    role: {
      update: '/role/', // PUT
      query: '/role/query', // POST
      queryDetail: '/role/', // GET
      delete: '/role/', // DELETE
      create: '/role', // POST
      dataexport: '/dataexport/role'
    },
    permission: {
      update: '/permission/', // PUT
      query: '/permission/get', // POST
      delete: '/permission/', // DELETE
      create: '/permission/a', // POST
      dataexport: '/dataexport/permission',
      queryItemFunction: '/permission/item/function/relation/q', // POST
      queryItemFunctionByRoleId: '/permission/item/function/relation/role/', // POST
      queryItemFunctionByUserId: '/permission/item/function/relation/user/' // POST
    },
    permissionFunction: {
      update: '/permission/function/', // PUT
      query: '/permission/function/q', // POST
      queryByPermissionId: '/permission/function/', // POST
      delete: '/permission/function/', // DELETE
      create: '/permission/function/a', // POST
      dataexport: '/dataexport/permission/function'
    },
    settingMenu: {
      update: '/menu/', // PUT
      query: '/menu/q', // POST
      delete: '/menu/', // DELETE
      create: '/menu/a' // POST
    },
    rolePermissionFunction: {
      update: '/role/permission/function/', // PUT
      query: '/role/permission/function/q', // POST
      delete: '/role/permission/function/', // DELETE
      create: '/role/permission/function/a', // POST
      dataexport: '/dataexport/role/permission/function'
    },
    userPermissionFunction: {
      update: '/user/permission/function/', // PUT
      query: '/user/permission/function/q', // POST
      delete: '/user/permission/function/', // DELETE
      create: '/user/permission/function/a', // POST
      dataexport: '/dataexport/user/permission/function'
    },
    roleUser: {
      update: '/role/user/', // PUT
      query: '/role/user/q', // POST
      delete: '/role/', // DELETE
      create: '/role/user/a' // POST
    },
    stocktake: {
      update: '/stocktake/', // PUT
      query: '/stocktake/query', // POST
      queryDetail: '/stocktake/and/detail', // POST
      getStocktakeInventory: '/get/stocktake/inventory', // 取得要盤點資料
      delete: '/stocktake/batch/delete', // POST
      create: '/stocktake', // POST
      start: '/stocktake/start/now', // 啟動盤點
      assign: '/stocktake/assign/user', // 指派盤點員
      complete: '/stocktake/complete/first', // 完成初盤
      confirm: '/stocktake/confirm', // 確認盤點調整庫存
      dataexport: '/stocktake/detail/excel/export', // 盤點單 - 匯出 Excel, CSV
      upload: '/fileupload/dataimport', // 資料上傳
      stocktakeCountStatus: '/stocktake/count/status' // 查詢各狀態總量
    },
    stocktakeDetail: {
      update: '/inventory/stocktake/detail/', // PUT
      query: '/inventory/stocktake/detail/q', // POST
      delete: '/stocktake/detail/batch/delete', // POST
      create: '/inventory/stocktake/detail/a', // POST
      updateAssign: '/inventory/stocktake/assign/', // PUT
      createAssign: '/inventory/stocktake/detail/asign/a', // POST
      dataexport: '/stocktake/detail/excel/export' // 盤點單 - 匯出 Excel, CSV
    },
    quotation: {
      update: '/quotation/', // PUT
      query: '/quotation/q', // POST
      delete: '/quotation/', // DELETE
      create: '/quotation/a', // POST
      bulk: '/quotation/bulk' // POST
    },
    quotationDetail: {
      update: '/quotation/detail/', // PUT
      query: '/quotation/detail/q', // POST
      delete: '/quotation/detail/', // DELETE
      create: '/quotation/detail/a' // POST
    },
    settingfee: {
      update: '/settingfee/', // PUT
      query: '/settingfee/q', // POST
      delete: '/settingfee/', // DELETE
      create: '/settingfee/a' // POST
    },
    productRelation: {
      update: '/product/relation/', // PUT
      query: '/product/relation/', // POST
      delete: '/product/relation/', // DELETE
      create: '/product/relation/a' // POST
    },
    bill: {
      update: '/bill/', // PUT
      query: '/bill/q', // POST
      delete: '/bill/', // DELETE
      cancele: '/bill/cancel/', // POST
      create: '/bill/a', // POST
      autoDetail: '/bill/auto/detail/a', // 報價單轉帳單
      bulk: '/bill/bulk', // POST
      updateStatus: '/bill/update/status/', // POST
      dataexport: '/dataexport/bill/full', // 盤點單 - 匯出 Excel, CSV
      getBilledDetail: '/common/system/maintain/billed/detail/auto/a' // 產帳單費用
    },
    billDetail: {
      update: '/bill/detail/', // PUT
      query: '/bill/detail/q', // POST
      delete: '/bill/detail/', // DELETE
      create: '/bill/detail/a' // POST
    },
    dashboard: {
      query: '/dashboard/q' // POST
    },
    exportSetup: {
      xmlQuery: '/xml/export-setting/q', // POST
      xmlExport: '/xml/export/e', // POST
      excelQuery: '/excel/export-setting/q', // POST
      excelExport: '/excel/export/e', // POST
      csvQuery: '/csv/export-setting/q', // POST
      csvExport: '/csv/export/e', // POST
      jsonQuery: '/json/export-setting/q', // POST
      jsonExport: '/json/export/e' // POST
    },
    announce: {
      update: '/announce/', // PUT
      query: '/announce/', // POST
      delete: '/announce/', // DELETE
      create: '/announce/a', // POST
      upload: '/fileupload/announce' // 資料上傳
    },
    memo: {
      update: '/memo/', // PUT
      query: '/memo/q', // POST
      delete: '/memo/', // DELETE
      create: '/memo/a', // POST
      upload: '/fileupload/memo' // 資料上傳
    },
    printtag: {
      product: '/printtag/product', // POST
      storage: '/printtag/storage' // POST
    },
    tagsetting: {
      update: '/tagsetting/', // PUT
      query: '/tagsetting/', // POST
      delete: '/tagsetting/', // DELETE
      create: '/tagsetting/a' // POST
    },
    productNutrition: {
      update: '/product/nutrition/', // PUT
      query: '/product/nutrition/', // POST
      delete: '/product/nutrition/', // DELETE
      create: '/product/nutrition/a', // POST
      printtag: '/printtag/product/nutrition'
    },
    gridLayout: {
      query: '/get/grid/layout/', // GET
      create: '/layout' // POST
    },
    report: {
      query: '/report/list/get/', // GET
      print: '/print/report' // POST
    },
    stockUnit: {
      query: '/stock/unit/query' // POST
    },
    weightUnit: {
      query: '/weight/unit/query' // POST
    },
    lengthUnit: {
      query: '/length/unit/query' // POST
    },
    pickingByConsignee: {
      update: '/pickingList/consignee/num/update', // POST
      query: '/pickingList/consignee/query/for/web', // POST
      queryAllList: '/pickingList/consignee/query/forApp' // POST
    }
  }
}
export default config
