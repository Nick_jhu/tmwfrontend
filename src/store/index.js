import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

//
import i18n from './modules/i18n'
import auth from './modules/auth'
import companyGroup from './modules/companyGroup'
import company from './modules/company'
import department from './modules/department'
import warehouse from './modules/warehouse'
import basicParameter from './modules/basicParameter'
import order from './modules/order'
import customer from './modules/customer'
import contact from './modules/contact'
import user from './modules/user'
import custuser from './modules/custuser'
import car from './modules/car'
import dlvmission from './modules/dlvmission'
import userQueryByCompanyIdWhId from './modules/userQueryByCompanyIdWhId'
import userUi from './modules/userUi'
import country from './modules/country'
import product from './modules/product'
import noVirtualProduct from './modules/noVirtualProduct'
import withoutInProductRelationParent from './modules/withoutInProductRelationParent'
import customerCategory from './modules/customerCategory'
import code from './modules/code'
import settingCode from './modules/settingCode'
import settingCodeByCompanyCodeType from './modules/settingCodeByCompanyCodeType'
import unit from './modules/unit'
import settingUnit from './modules/settingUnit'
import stateProvince from './modules/stateProvince'
import city from './modules/city'
import district from './modules/district'
import region from './modules/region'
import storage from './modules/storage'
import storageDetail from './modules/storageDetail'
import storageArea from './modules/storageArea'
import account from './modules/account'
import companyUser from './modules/companyUser'
import warehouseUser from './modules/warehouseUser'
import inbound from './modules/inbound'
import productCategory from './modules/productCategory'
import putInOperationSimple from './modules/putInOperationSimple'
import putInOperation from './modules/putInOperation'
import putInQuality from './modules/putInQuality'
import putInShelves from './modules/putInShelves'
import inboundSnapshot from './modules/inboundSnapshot'
import returnOperationSimple from './modules/returnOperationSimple'
import returnOperation from './modules/returnOperation'
import returnAcceptance from './modules/returnAcceptance'
import returnShelves from './modules/returnShelves'
import returnSnapshot from './modules/returnSnapshot'
import inboundDetail from './modules/inboundDetail'
import inventory from './modules/inventory'
import putInOperationSimpleInventory from './modules/putInOperationSimpleInventory'
import putInQualityInventory from './modules/putInQualityInventory'
import putInShelvesInventory from './modules/putInShelvesInventory'
import inventoryQuery from './modules/inventoryQuery'
import inboundDetailPutaway from './modules/inboundDetailPutaway'
import returnOperationSimpleInventory from './modules/returnOperationSimpleInventory'
import returnOperationInventory from './modules/returnOperationInventory'
import returnAcceptanceInventory from './modules/returnAcceptanceInventory'
import returnShelvesInventory from './modules/returnShelvesInventory'
import returnInventoryQuery from './modules/returnInventoryQuery'
import adjustment from './modules/adjustment'
import custom from './modules/custom'
import currencyCode from './modules/currencyCode'
import outboundOperation from './modules/outboundOperation'
import outboundQueryByCompanyWh from './modules/outboundQueryByCompanyWh'
import fastBoxing from './modules/fastBoxing'
import pickingOperationOutbound from './modules/pickingOperationOutbound'
import outboundDetail from './modules/outboundDetail'
import orderDetail from './modules/orderDetail'
import outboundDetailSnapshot from './modules/outboundDetailSnapshot'
import pickingConfirmOutbound from './modules/pickingConfirmOutbound'
import outboundDetailQuery from './modules/outboundDetailQuery'
import outboundSnapshot from './modules/outboundSnapshot'
import pickinglist from './modules/pickinglist'
import shippinglist from './modules/shippinglist'
import stockProduct from './modules/stockProduct'
import stockStorage from './modules/stockStorage'
import stockStorageBydate from './modules/stockStorageBydate'
import stockStorageBySummary from './modules/stockStorageBySummary'
import stockStorageByAccurate from './modules/stockStorageByAccurate'
import deliveryConfirm from './modules/deliveryConfirm'
import importPutIn from './modules/importPutIn'
import importOutbound from './modules/importOutbound'
import importCustomer from './modules/importCustomer'
import importGoods from './modules/importGoods'
import importReturn from './modules/importReturn'
import pdfmerge from './modules/pdfmerge'
import unitConvert from './modules/unitConvert'
import transfer from './modules/transfer'
import simpleProcessing from './modules/simpleProcessing'
import quantityinspection from './modules/quantityinspection'
import storageChange from './modules/storageChange'
import inventoryAndOutboundDetailQuery from './modules/inventoryAndOutboundDetailQuery'
import inboundAdjustment from './modules/inboundAdjustment'
import outboundAdjustment from './modules/outboundAdjustment'
import inventoryQueryFiftyByOwner from './modules/inventoryQueryFiftyByOwner'
import productQueryByOwner from './modules/productQueryByOwner'
import boxingSetup from './modules/boxingSetup'
import externalStockProduct from './modules/externalStockProduct'
import stockByProduct from './modules/stockByProduct'
import stockByStorage from './modules/stockByStorage'
import storageQueryByWhId from './modules/storageQueryByWhId'
import customerByCmpWhCat from './modules/customerByCmpWhCat'
import customerByCmpCat from './modules/customerByCmpCat'
import pickingListQuery from './modules/pickingListQuery'
import productSn from './modules/productSn'
import hctWaybill from './modules/hctWaybill'
import kerrytjWaybill from './modules/kerrytjWaybill'
import tongyingWaybill from './modules/tongyingWaybill'
import printwaybill from './modules/printwaybill'
import inventorySnapshot from './modules/inventorySnapshot'
import inventoryBooking from './modules/inventoryBooking'
import boxDetailsQuery from './modules/boxDetailsQuery'
import role from './modules/role'
import permission from './modules/permission'
import permissionFunction from './modules/permissionFunction'
import settingMenu from './modules/settingMenu'
import rolePermissionFunction from './modules/rolePermissionFunction'
import roleUser from './modules/roleUser'
import stocktake from './modules/stocktake'
import stocktakeDetail from './modules/stocktakeDetail'
import quotation from './modules/quotation'
import quotationDetail from './modules/quotationDetail'
import settingfee from './modules/settingfee'
import productRelation from './modules/productRelation'
import bill from './modules/bill'
import billDetail from './modules/billDetail'
import dashboard from './modules/dashboard'
import exportSetup from './modules/exportSetup'
import announce from './modules/announce'
import memo from './modules/memo'
import printtag from './modules/printtag'
import tagsetting from './modules/tagsetting'
import productNutrition from './modules/productNutrition'
import gridLayout from './modules/gridLayout'
import report from './modules/report'
import customerOwner from './modules/customerOwner'
import customerStore from './modules/customerStore'
import stockUnit from './modules/stockUnit'
import weightUnit from './modules/weightUnit'
import lengthUnit from './modules/lengthUnit'
import pickingByConsignee from './modules/pickingByConsignee'
//
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    i18n,
    auth,
    companyGroup,
    company,
    department,
    warehouse,
    product,
    noVirtualProduct,
    withoutInProductRelationParent,
    basicParameter,
    order,
    customer,
    customerCategory,
    code,
    settingCode,
    settingCodeByCompanyCodeType,
    unit,
    settingUnit,
    contact,
    user,
    custuser,
    car,
    dlvmission,
    userQueryByCompanyIdWhId,
    userUi,
    country,
    stateProvince,
    city,
    district,
    region,
    storage,
    storageDetail,
    storageArea,
    account,
    companyUser,
    warehouseUser,
    inbound,
    productCategory,
    putInOperationSimple,
    putInOperation,
    putInQuality,
    putInShelves,
    inboundSnapshot,
    returnOperationSimple,
    returnOperation,
    returnAcceptance,
    returnShelves,
    returnSnapshot,
    inboundDetail,
    inventory,
    putInOperationSimpleInventory,
    putInQualityInventory,
    putInShelvesInventory,
    inventoryQuery,
    inboundDetailPutaway,
    returnOperationSimpleInventory,
    returnOperationInventory,
    returnAcceptanceInventory,
    returnShelvesInventory,
    returnInventoryQuery,
    adjustment,
    custom,
    currencyCode,
    outboundOperation,
    outboundQueryByCompanyWh,
    fastBoxing,
    pickingOperationOutbound,
    outboundDetail,
    orderDetail,
    outboundDetailSnapshot,
    pickingConfirmOutbound,
    outboundDetailQuery,
    outboundSnapshot,
    pickinglist,
    shippinglist,
    stockProduct,
    stockStorage,
    stockStorageBydate,
    stockStorageBySummary,
    stockStorageByAccurate,
    deliveryConfirm,
    importPutIn,
    importOutbound,
    importCustomer,
    importGoods,
    importReturn,
    pdfmerge,
    unitConvert,
    transfer,
    simpleProcessing,
    quantityinspection,
    storageChange,
    inventoryAndOutboundDetailQuery,
    inboundAdjustment,
    outboundAdjustment,
    inventoryQueryFiftyByOwner,
    productQueryByOwner,
    boxingSetup,
    externalStockProduct,
    stockByProduct,
    stockByStorage,
    storageQueryByWhId,
    customerByCmpWhCat,
    customerByCmpCat,
    pickingListQuery,
    productSn,
    hctWaybill,
    kerrytjWaybill,
    tongyingWaybill,
    printwaybill,
    inventorySnapshot,
    inventoryBooking,
    boxDetailsQuery,
    role,
    permission,
    permissionFunction,
    settingMenu,
    rolePermissionFunction,
    roleUser,
    stocktake,
    stocktakeDetail,
    quotation,
    quotationDetail,
    settingfee,
    productRelation,
    bill,
    billDetail,
    dashboard,
    exportSetup,
    announce,
    memo,
    printtag,
    tagsetting,
    productNutrition,
    gridLayout,
    report,
    customerOwner,
    customerStore,
    stockUnit,
    weightUnit,
    lengthUnit,
    pickingByConsignee
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
