import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  pdfmerge ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pdfmerge.pdfmerge}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('pdfmerge', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  pdfmerge (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
