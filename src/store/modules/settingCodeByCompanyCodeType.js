import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  settingCodeByCompanyCodeTypeList: null,
  settingCodeByCompanyCodeType: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearSettingCodeByCompanyCodeTypeList ({ commit, state }, data) {
    commit('clearSettingCodeByCompanyCodeTypeList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.settingCodeByCompanyCodeType.query}${data.companyId}/${data.codeType}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.settingCode)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.settingCodeByCompanyCodeType.query}${data.companyId}/${data.codeType}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.settingCode)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.settingCodeByCompanyCodeType.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.settingCode)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.settingCodeByCompanyCodeType.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.respondCode.additionals.settingCode)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.settingCodeByCompanyCodeType.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.settingCodeByCompanyCodeType.query}${data.companyId}/${data.codeType}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.settingCode)
      return response.data
    }).catch((error) => {
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.settingCodeByCompanyCodeTypeList = null
    state.settingCodeByCompanyCodeType = null
  },
  clearSettingCodeByCompanyCodeTypeList (state) {
    state.settingCodeByCompanyCodeTypeList = null
  },
  query (state, data) {
    state.settingCodeByCompanyCodeTypeList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.settingCodeByCompanyCodeType = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.settingCodeByCompanyCodeType = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
