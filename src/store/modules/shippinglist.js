import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  generate ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.shippinglist.generate}/${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('generate', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  summary ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.shippinglist.summary}/${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('summary', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  hctWaybill ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.shippinglist.hctWaybill}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('hctWaybill', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  tongyingWaybill ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.shippinglist.tongyingWaybill}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('tongyingWaybill', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  generate (state) {
    // console.log(state)
  },
  summary (state) {
    // console.log(state)
  },
  hctWaybill (state) {
    // console.log(state)
  },
  tongyingWaybill (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
