import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  contactList: null,
  contact: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearContactList ({ commit, state }, data) {
    commit('clearContactList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.contact)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.contact)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.contact)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.update}${data.id}`
    return app.axios.put(api, data.data).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.contact.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.contact)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  upload ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.upload}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('upload', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  import ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.import}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.contact.dataexport}/${data.companyId}/${data.whId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.contactList = null
    state.contact = null
  },
  clearContactList (state) {
    state.contactList = null
  },
  query (state, data) {
    state.contactList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.contact = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.contact = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  upload (state) {
    // console.log(state)
  },
  import (state) {
    // console.log(state)
  },
  dataexport (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
