import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  storageQueryByWhIdList: null,
  storageQueryByWhId: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStorageQueryByWhIdList ({ commit, state }, data) {
    commit('clearStorageQueryByWhIdList')
  },
  clearStorageQueryByWhId ({ commit, state }, data) {
    commit('clearStorageQueryByWhId')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storageQueryByWhId.query}${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.storage)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storageQueryByWhId.query}${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.storage)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storageQueryByWhId.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.storage)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.storageQueryByWhId.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storageQueryByWhId.update}${data.storageId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.httpStatus)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.storageQueryByWhIdList = null
    state.storageQueryByWhId = null
  },
  clearStorageQueryByWhIdList (state) {
    state.storageQueryByWhIdList = null
  },
  clearStorageQueryByWhId (state) {
    state.storageQueryByWhId = null
  },
  query (state, data) {
    state.storageQueryByWhIdList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
