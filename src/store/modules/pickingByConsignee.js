import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  pickingByConsigneeList: null,
  pickingByConsignee: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPickingByConsigneeList ({ commit, state }, data) {
    commit('clearPickingByConsigneeList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingByConsignee.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingByConsignee.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryAllList ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingByConsignee.queryAllList}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryAllList', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingByConsignee.update}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
  // queryDetail ({ commit, state }, id) {
  //   // console.log(data)
  //   let api = `${apiBaseUrl}${Config.API.pickingByConsignee.queryDetail}${id}`
  //   return app.axios.get(api).then((response) => {
  //     console.log(response.data)
  //     commit('queryDetail', response.data)
  //     return response.data
  //   }).catch((error) => {
  //     // console.log(error.response)
  //     return error.response
  //   })
  // },
  // create ({ commit, state }, data) {
  //   // console.log(data)
  //   let api = `${apiBaseUrl}${Config.API.pickingByConsignee.create}`
  //   return app.axios.post(api, data).then((response) => {
  //     // console.log(response.data)
  //     commit('create', response.data.data)
  //     return response.data
  //   }).catch((error) => {
  //     // console.log(error.response)
  //     return error.response
  //   })
  // },
  // delete ({ commit, state }, data) {
  //   // console.log(id)
  //   let api = `${apiBaseUrl}${Config.API.pickingByConsignee.delete}`
  //   return app.axios.delete(api, { data: data }).then((response) => {
  //     // console.log(response.data)
  //     commit('delete', response.data)
  //     return response.data
  //   }).catch((error) => {
  //     // console.log(error.response)
  //     return error.response.data
  //   })
  // },
  // dataexport ({ commit, state }, data) {
  //   // console.log(data)
  //   let api = `${apiBaseUrl}${Config.API.pickingByConsignee.dataexport}`
  //   return app.axios.post(api, data).then((response) => {
  //     // console.log(response.data)
  //     commit('dataexport', response.data)
  //     return response.data
  //   }).catch((error) => {
  //     // console.log(error.response)
  //     return error.response
  //   })
  // },
  // import ({ commit, state }, data) {
  //   // console.log(data)
  //   let api = `${apiBaseUrl}${Config.API.pickingByConsignee.import}`
  //   return app.axios.post(api, data).then((response) => {
  //     // console.log(response.data)
  //     commit('import', response.data)
  //     return response.data
  //   }).catch((error) => {
  //     // console.log(error.response)
  //     return error.response
  //   })
  // }
}

// mutations
const mutations = {
  clear (state) {
    state.pickingByConsigneeList = null
    state.pickingByConsignee = null
  },
  clearPickingByConsigneeList (state) {
    state.pickingByConsigneeList = null
  },
  query (state, data) {
    state.pickingByConsigneeList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryAllList (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  }
  // queryDetail (state, data) {
  //   state.pickingByConsignee = data.pickingByConsignee
  // },
  // clearDetail (state, data) {
  //   state.pickingByConsignee = null
  // },
  // create (state, data) {
  //   // console.log(data)
  // },
  // delete (state, data) {
  //   // console.log('delete', data)
  // },
  // dataexport (state) {
  //   // console.log(state)
  // },
  // import (state) {
  //   // console.log(state)
  // }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
