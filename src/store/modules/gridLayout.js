import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  gridLayoutList: null,
  gridLayout: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearGridLayoutList ({ commit, state }, data) {
    commit('clearGridLayoutList')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.gridLayout.query}${data}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.gridLayout.query}${data}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.gridLayout.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.gridLayoutList = null
    state.gridLayout = null
  },
  clearGridLayoutList (state) {
    state.gridLayoutList = null
  },
  query (state, data) {
    // console.log(data)
    state.gridLayoutList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
