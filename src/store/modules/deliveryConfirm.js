import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  deliveryConfirmList: null,
  deliveryConfirm: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearDeliveryConfirmList ({ commit, state }, data) {
    commit('clearDeliveryConfirmList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.update}${data.id}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  deliveryConfirmAutoBoxing ({ commit, state }) {
    let api = `${apiBaseUrl}${Config.API.deliveryConfirm.deliveryConfirmAutoBoxing}`
    return app.axios.post(api).then((response) => {
      // console.log(response.data)
      commit('deliveryConfirmAutoBoxing', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.deliveryConfirmList = null
    state.deliveryConfirm = null
  },
  clearDeliveryConfirmList (state) {
    state.deliveryConfirmList = null
  },
  query (state, data) {
    state.deliveryConfirmList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.deliveryConfirm = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.deliveryConfirm = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  deliveryConfirmAutoBoxing (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
