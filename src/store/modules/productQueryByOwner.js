import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  productQueryByOwnerList: null,
  productQueryByOwner: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearProductQueryByOwnerList ({ commit, state }, data) {
    commit('clearProductQueryByOwnerList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productQueryByOwner.query}${data.companyId}/owner/${data.ownerId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productQueryByOwner.query}${data.companyId}/owner/${data.ownerId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productQueryByOwner.query}${data.companyId}/owner/${data.ownerId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productQueryByOwner.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.productQueryByOwner.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productQueryByOwner.update}${data.id}`
    return app.axios.put(api, data.data).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.productQueryByOwnerList = null
    state.productQueryByOwner = null
  },
  clearProductQueryByOwnerList (state) {
    state.productQueryByOwnerList = null
  },
  query (state, data) {
    state.productQueryByOwnerList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.productQueryByOwner = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.productQueryByOwner = null
  },
  create (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  update (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
