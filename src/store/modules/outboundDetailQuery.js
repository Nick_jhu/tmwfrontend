import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  outboundDetailQueryList: null,
  outboundDetailQuery: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearOutboundDetailQueryList ({ commit, state }, data) {
    commit('clearOutboundDetailQueryList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundDetailQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundDetailQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryMoreThanMonth ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundDetailQuery.queryMoreThanMonth}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryMoreThanMonth', response.data.respondCode.additionals.outboundDetailHistoryList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundDetailQuery.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.outboundDetailQueryList = null
    state.outboundDetailQuery = null
  },
  clearOutboundDetailQueryList (state) {
    state.outboundDetailQueryList = null
  },
  query (state, data) {
    // console.log(data)
    state.outboundDetailQueryList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryMoreThanMonth (state, data) {
    // console.log(data)
    state.outboundDetailQueryList = data
  },
  dataexport (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
