import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  customerByCmpWhCatList: null,
  customerByCmpWhCat: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearCustomerByCmpWhCatList ({ commit, state }, data) {
    commit('clearCustomerByCmpWhCatList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.customerByCmpWhCat.query}${data.companyId}/${data.whId}/bycat/${data.customerCat}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.customer)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.customerByCmpWhCat.query}${data.companyId}/${data.whId}/bycat/${data.customerCat}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.customer)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.customerByCmpWhCat.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.customer)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.customerByCmpWhCat.update}${data.id}`
    return app.axios.put(api, data.data).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.customerByCmpWhCat.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  async queryDetail ({ commit, state }, data) {
    // console.log(data)
    try {
      let api = `${apiBaseUrl}${Config.API.customerByCmpWhCat.query}${data.companyId}/${data.whId}/bycat/${data.customerCat}/q`
      let customerRes = await app.axios.post(api, data.postData)
      var customerResponse = customerRes.data.respondCode.additionals.customer
      let customerCategoryRes = await app.axios.post(`${apiBaseUrl}${Config.API.customerCategory.query}`, data.postData)
      if (customerCategoryRes.data.respondCode.additionals.customerCategory.length > 0) {
        customerResponse[0].customer_cat = customerCategoryRes.data.respondCode.additionals.customerCategory[0].customer_cat
        customerResponse[0].cc_id = customerCategoryRes.data.respondCode.additionals.customerCategory[0].cc_id
      }
      commit('queryDetail', customerResponse)
      return customerResponse
      // return app.axios.post(api, data).then((response) => {
      //   // console.log(response.data)
      //   commit('queryDetail', response.data.respondCode.additionals.customer)
      //   return response.data
      // }).catch((error) => {
      //   // console.log(error.response)
      //   return error.response
      // })
    } catch (err) {
      // console.log(err)
    }
  }
}

// mutations
const mutations = {
  clear (state) {
    state.customerByCmpWhCatList = null
    state.customerByCmpWhCat = null
  },
  clearCustomerByCmpWhCatList (state) {
    state.customerByCmpWhCatList = null
  },
  query (state, data) {
    state.customerByCmpWhCatList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.customerByCmpWhCat = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.customerByCmpWhCat = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
