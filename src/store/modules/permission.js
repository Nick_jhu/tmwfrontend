import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  permissionList: null,
  permission: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPermissionList ({ commit, state }, data) {
    commit('clearPermissionList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.query}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.query}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.permissionItemList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.permission.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.permissionItemList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.dataexport}/${data.companyId}/${data.whId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryItemFunction ({ commit, state }) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.queryItemFunction}`
    return app.axios.post(api).then((response) => {
      console.log(response.data)
      commit('queryItemFunction', response.data.respondCode.additionals.permissionItemFunction)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryItemFunctionByRoleId ({ commit, state }, id) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.queryItemFunctionByRoleId}${id}/q`
    return app.axios.post(api).then((response) => {
      console.log(response.data)
      commit('queryItemFunctionByRoleId', response.data.respondCode.additionals.permissionItemFunction)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryItemFunctionByUserId ({ commit, state }, id) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.permission.queryItemFunctionByUserId}${id}/q`
    return app.axios.post(api).then((response) => {
      console.log(response.data)
      commit('queryItemFunctionByUserId', response.data.respondCode.additionals.permissionItemFunction)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.permissionList = null
    state.permission = null
  },
  clearPermissionList (state) {
    state.permissionList = null
  },
  query (state, data) {
    state.permissionList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.permission = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.permission = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  dataexport (state) {
    // console.log(state)
  },
  queryItemFunction (state, data) {
    // console.log(data)
  },
  queryItemFunctionByRoleId (state, data) {
    // console.log(data)
  },
  queryItemFunctionByUserId (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
