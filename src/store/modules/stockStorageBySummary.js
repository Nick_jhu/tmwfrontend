import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  stockStorageBySummaryList: null,
  stockStorageBySummary: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStockStorageBySummaryList ({ commit, state }, data) {
    commit('clearStockStorageBySummaryList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageBySummary.query}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageBySummary.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageBySummary.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageBySummary.dataexport}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.stockStorageBySummaryList = null
    state.stockStorageBySummary = null
  },
  clearStockStorageBySummaryList (state) {
    state.stockStorageBySummaryList = null
  },
  query (state, data) {
    // console.log(data)
    state.stockStorageBySummaryList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.stockStorageBySummary = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.stockStorageBySummary = null
  },
  dataexport (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
