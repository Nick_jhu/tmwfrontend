import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  stocktakeList: null,
  stocktake: null,
  stocktakeDetailList: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStocktakeList ({ commit, state }, data) {
    commit('clearStocktakeList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.queryDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  getStocktakeInventory ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.getStocktakeInventory}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('getStocktakeInventory', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.create}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('create', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  start ({ commit, state }, data) {
    let api = `${apiBaseUrl}${Config.API.stocktake.start}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('start', response.data.data)
      return response.data
    }).catch((error) => {
      console.log(error.response)
      return error.response
    })
  },
  assign ({ commit, state }, data) {
    let api = `${apiBaseUrl}${Config.API.stocktake.assign}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('assign', response.data.data)
      return response.data
    }).catch((error) => {
      console.log(error.response)
      return error.response
    })
  },
  complete ({ commit, state }, data) {
    let api = `${apiBaseUrl}${Config.API.stocktake.complete}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('complete', response.data.data)
      return response.data
    }).catch((error) => {
      console.log(error.response)
      return error.response
    })
  },
  confirm ({ commit, state }, data) {
    let api = `${apiBaseUrl}${Config.API.stocktake.confirm}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('confirm', response.data.data)
      return response.data
    }).catch((error) => {
      console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.stocktake.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.dataexport}/${data.companyId}/${data.whId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  upload ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.upload}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('upload', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  stocktakeCountStatus ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stocktake.stocktakeCountStatus}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('stocktakeCountStatus', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.stocktakeList = null
    state.stocktake = null
    state.stocktakeDetailList = null
  },
  clearStocktakeList (state) {
    state.stocktakeList = null
  },
  query (state, data) {
    state.stocktakeList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.stocktake = data.recStocktake || null
    state.stocktakeDetailList = data.stocktakeDetails || []
  },
  getStocktakeInventory (state, data) {
    // console.log(data)
  },
  clearDetail (state, data) {
    state.stocktake = null
    state.stocktakeDetailList = null
  },
  create (state, data) {
    // console.log(data)
  },
  start (state, data) {
    // console.log(data)
  },
  assign (state, data) {
    // console.log(data)
  },
  complete (state, data) {
    // console.log(data)
  },
  confirm (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  dataexport (state) {
    // console.log(state)
  },
  upload (state) {
    // console.log(state)
  },
  stocktakeCountStatus (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
