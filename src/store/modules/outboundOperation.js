import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  outboundOperationList: null,
  outboundOperation: null,
  orderDetail: null,
  outboundDetail: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearOutboundOperationList ({ commit, state }, data) {
    commit('clearOutboundOperationList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.queryDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  booking ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.booking}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('booking', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  sendtoprint ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.sendtoprint}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('sendtoprint', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  recovery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.recovery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('recovery', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  recoveryBooking ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.recoveryBooking}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('recoveryBooking', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  confirm ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.confirm}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('confirm', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  batchUpdateStatus ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.batchUpdateStatus}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('batchUpdateStatus', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  pickinglistByOrder ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.pickinglistByOrder}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('pickinglistByOrder', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  outofstockDataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.outofstockDataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('outofstockDataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  dataexportSnapshot ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.dataexportSnapshot}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexportSnapshot', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  import ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.import}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  productsnImport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.productsnImport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('productsnImport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  updateBatch ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.updateBatch}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('updateBatch', response.data.respondCode.additionals)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  outboundCountStatus ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.outboundCountStatus}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('outboundCountStatus', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  outboundQc ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.outboundQc}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('outboundQc', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  outboundQualityCheck ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.outboundQualityCheck}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('outboundQualityCheck', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  printQualityCheck ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.printQualityCheck}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('printQualityCheck', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  orderDetailBatchDeleteByProduct ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.orderDetailBatchDeleteByProduct}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('orderDetailBatchDeleteByProduct', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  outboundToInbound ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.outboundOperation.outboundToInbound}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('outboundToInbound', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.outboundOperationList = null
    state.outboundOperation = null
  },
  clearOutboundOperationList (state) {
    state.outboundOperationList = null
  },
  query (state, data) {
    state.outboundOperationList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    console.log(data)
    state.outboundOperation = data.recOutbound || null
    state.orderDetail = data.orderDetail || []
    state.outboundDetail = data.outboundDetail || []
  },
  clearDetail (state, data) {
    state.outboundOperation = null
    state.orderDetail = null
    state.outboundDetail = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  booking (state, data) {
    // console.log('booking', data)
  },
  sendtoprint (state, data) {
    // console.log('sendtoprint', data)
  },
  recovery (state, data) {
    // console.log('recovery', data)
  },
  recoveryBooking (state, data) {
    // console.log('recoveryBooking', data)
  },
  confirm (state, data) {
    // console.log(data)
  },
  batchUpdateStatus (state, data) {
    // console.log(data)
  },
  pickinglistByOrder (state, data) {
    // console.log(data)
  },
  dataexport (state) {
    // console.log(state)
  },
  outofstockDataexport (state) {
    // console.log(state)
  },
  dataexportSnapshot (state) {
    // console.log(state)
  },
  import (state) {
    // console.log(state)
  },
  productsnImport (state) {
    // console.log(state)
  },
  updateBatch (state, data) {
    // console.log(data)
  },
  outboundCountStatus (state, data) {
    // console.log(data)
  },
  outboundQc (state, data) {
    // console.log(data)
  },
  outboundQualityCheck (state, data) {
    // console.log(data)
  },
  printQualityCheck (state, data) {
    // console.log(data)
  },
  orderDetailBatchDeleteByProduct (state, data) {
    // console.log(data)
  },
  outboundToInbound (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
