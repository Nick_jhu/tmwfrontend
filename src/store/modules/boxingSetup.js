import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  cityList: null,
  city: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  getOutboundNoId ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.getOutboundNoId}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('getOutboundNoId', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  getOutboundNoIdByCompanyWh ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.getOutboundNoIdByCompanyWh}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('getOutboundNoIdByCompanyWh', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.query}${data.outboundId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.products_to_box)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  addBoxing ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.addBoxing}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('addBoxing', response.data.respondCode.additionals.recBoxing)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  addBoxingDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.addBoxingDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('addBoxingDetail', response.data.respondCode.additionals)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  queryBoxing ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.queryBoxing}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryBoxing', response.data.respondCode.additionals.recBoxingList)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  deleteBoxing ({ commit, state }, boxingId) {
    // console.log(boxingId)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.deleteBoxing}${boxingId}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('deleteBoxing', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  queryBoxingDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.queryBoxingDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryBoxingDetail', response.data.respondCode.additionals.boxDetailList)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  deleteBoxingDetail ({ commit, state }, prodId) {
    // console.log(prodId)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.deleteBoxingDetail}${prodId}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('deleteBoxingDetail', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  updateBoxing ({ commit, state }, data) {
    // console.log(prodId)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.updatBoxing}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('updateBoxing', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  updateBoxingDetail ({ commit, state }, data) {
    // console.log(prodId)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.updateBoxingDetail}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('updateBoxingDetail', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  comfirmBoxing ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.comfirmBoxing}${data.id}`
    return app.axios.put(api, data.data).then((response) => {
      // console.log(response.data)
      commit('comfirmBoxing', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryPickinglist ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.queryPickinglist}${data.pickinglistNo}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryPickinglist', response.data.respondCode.additionals.pickinglist_byorder)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  boxingReport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.boxingReport}${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryProductSn ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.queryProductSn}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryProductSn', response.data.respondCode.additionals.productSnNumsByBoxIdsOutboundIds)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  checkProductSn ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxingSetup.checkProductSn}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('checkProductSn', response.data.respondCode.additionals.productSnNumsEqualByOutboundIds)
      return response.data
    }).catch((error) => {
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    // console.log('clear')
  },
  getOutboundNoId (state, data) {
    // console.log(data)
  },
  getOutboundNoIdByCompanyWh (state, data) {
    // console.log(data)
  },
  query (state, data) {
    // console.log(data)
  },
  addBoxing (state, data) {
    // console.log(data)
  },
  addBoxingDetail (state, data) {
    // console.log(data)
  },
  queryBoxing (state, data) {
    // console.log(data)
  },
  deleteBoxing (state, data) {
    // console.log(data)
  },
  queryBoxingDetail (state, data) {
    // console.log(data)
  },
  deleteBoxingDetail (state, data) {
    // console.log(data)
  },
  updateBoxing (state, data) {
    // console.log(data)
  },
  updateBoxingDetail (state, data) {
    // console.log(data)
  },
  comfirmBoxing (state, data) {
    // console.log(data)
  },
  queryPickinglist (state, data) {
    // console.log(data)
  },
  boxingReport (state, data) {
    // console.log(data)
  },
  queryProductSn (state, data) {
    // console.log(data)
  },
  checkProductSn (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
