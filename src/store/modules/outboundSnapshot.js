import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  outboundSnapshotList: null,
  outboundSnapshot: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearOutboundSnapshotList ({ commit, state }, data) {
    commit('clearOutboundSnapshotList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.update}${data.id}`
    return app.axios.put(api, data.data).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryClose ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.queryClose}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryClose', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  createClose ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.createClose}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('createClose', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  recoveryClose ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundSnapshot.recoveryClose}`
    return app.axios.put(api, data).then((response) => {
      // console.log(response.data)
      commit('recoveryClose', response.data.respondCode.additionals.outboundSnapshotList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.outboundSnapshotList = null
    state.outboundSnapshot = null
  },
  clearOutboundSnapshotList (state) {
    state.outboundSnapshotList = null
  },
  query (state, data) {
    state.outboundSnapshotList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.outboundSnapshot = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.outboundSnapshot = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  queryClose (state, data) {
    // console.log('queryClose', data)
  },
  createClose (state, data) {
    // console.log('createClose', data)
  },
  recoveryClose (state, data) {
    // console.log('recoveryClose', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
