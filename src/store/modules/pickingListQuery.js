import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  pickingListQueryList: null,
  pickingListQuery: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPickingListQueryList ({ commit, state }, data) {
    commit('clearPickingListQueryList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingListQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.pickinglist_byorder)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingListQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.pickinglist_byorder)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickingListQuery.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.pickingListQueryList = null
    state.pickingListQuery = null
  },
  clearPickingListQueryList (state) {
    state.pickingListQueryList = null
  },
  query (state, data) {
    // console.log(data)
    state.pickingListQueryList = data
  },
  search (state, data) {
    // console.log(data)
  },
  dataexport (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
