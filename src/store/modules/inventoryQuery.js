import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  inventoryQueryList: null,
  inventoryQuery: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearInventoryQueryList ({ commit, state }, data) {
    commit('clearInventoryQueryList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryQuery.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryQuery.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  queryMoreThanMonth ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryQuery.queryMoreThanMonth}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryMoreThanMonth', response.data.respondCode.additionals.inboundDetailHistory)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryQuery.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryQuery.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryQuery.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.inventoryQueryList = null
    state.inventoryQuery = null
  },
  clearInventoryQueryList (state) {
    state.inventoryQueryList = null
  },
  query (state, data) {
    state.inventoryQueryList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryMoreThanMonth (state, data) {
    state.inventoryQueryList = data
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    state.inventoryQueryList = data
    // console.log(data)
  },
  dataexport (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
