import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  printtagList: null,
  printtag: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPrinttagList ({ commit, state }, data) {
    commit('clearPrinttagList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  product ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.printtag.product}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('product', response.data.respondCode.additionals.print_tag_product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  storage ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.printtag.storage}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('storage', response.data.respondCode.additionals.print_tag_storage)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.printtagList = null
    state.printtag = null
  },
  clearPrinttagList (state) {
    state.printtagList = null
  },
  product (state, data) {
    // console.log(data)
  },
  storage (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
