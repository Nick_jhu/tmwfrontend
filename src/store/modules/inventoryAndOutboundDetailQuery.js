import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  inventoryAndOutboundDetailQueryList: null,
  inventoryAndOutboundDetailQuery: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearInventoryList ({ commit, state }, data) {
    commit('clearInventoryList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    let api = `${apiBaseUrl}${Config.API.inventoryAndOutboundDetailQuery.query}`
    return app.axios.post(api, data).then((response) => {
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryAndOutboundDetailQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryAndOutboundDetailQuery.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.inventoryAndOutboundDetailQueryList = null
    state.inventoryAndOutboundDetailQuery = null
  },
  clearInventoryList (state) {
    state.inventoryAndOutboundDetailQueryList = null
  },
  query (state, data) {
    state.inventoryAndOutboundDetailQueryList = data
  },
  search (state, data) {
    // console.log(data)
  },
  dataexport (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
