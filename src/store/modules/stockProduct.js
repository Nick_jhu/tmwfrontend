import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  stockProductList: null,
  stockProduct: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStockProductList ({ commit, state }, data) {
    commit('clearStockProductList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockProduct.query}${data.companyId}/${data.whId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.stock_byproduct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockProduct.query}${data.companyId}/${data.whId}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.stock_byproduct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockProduct.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.stock_byproduct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockProduct.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.stock_byproduct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockProduct.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.stockProduct.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.stockProductList = null
    state.stockProduct = null
  },
  clearStockProductList (state) {
    state.stockProductList = null
  },
  query (state, data) {
    // console.log(data)
    state.stockProductList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.stockProduct = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.stockProduct = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
