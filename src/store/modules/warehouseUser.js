import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  warehouseUserList: null,
  warehouseUser: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearWarehouseUserList ({ commit, state }, data) {
    commit('clearWarehouseUserList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.warehouseUser.update}${data.id}`
    return app.axios.put(api, data.item).then((response) => {
      // console.log(response.data)
      commit('update', response.data.respondCode.additionals.warehouseUser)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.warehouseUser.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.warehouseUser)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.warehouseUser.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.warehouseUser)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.warehouseUser.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.warehouseUser)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.warehouseUser.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.warehouseUser)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, data) {
    // console.log('data?????????????????????????????????????')
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.warehouseUser.delete}${data.wh_id}/user/${data.user_id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.warehouseUserList = null
    state.warehouseUser = null
  },
  clearWarehouseUserList (state) {
    state.warehouseUserList = null
  },
  query (state, data) {
    state.warehouseUserList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.warehouseUser = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.warehouseUser = null
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  update (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
