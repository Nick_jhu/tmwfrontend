import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  productNutritionList: null,
  productNutrition: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearProductNutritionList ({ commit, state }, data) {
    commit('clearProductNutritionList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productNutrition.query}${data.productId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.productNutrition)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productNutrition.query}${data.productId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.productNutrition)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productNutrition.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.productNutrition)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productNutrition.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.respondCode)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.productNutrition.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productNutrition.query}${data.productId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.productNutrition)
      return response.data
    }).catch((error) => {
      return error.response.data
    })
  },
  printtag ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productNutrition.printtag}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('printtag', response.data.respondCode.additionals.print_tag_product_nutrition)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.productNutritionList = null
    state.productNutrition = null
  },
  clearProductNutritionList (state) {
    state.productNutritionList = null
  },
  query (state, data) {
    state.productNutritionList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.productNutrition = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.productNutrition = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  printtag (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
