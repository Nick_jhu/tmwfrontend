// 匯入我們的Vue Instance
import { app } from '../../main'

// initial state
const state = {
  lang: 'zh_TW'
}

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, lang) {
    commit('setLang', 'zh_TW')
  },
  setLang ({ commit, state }, lang) {
    commit('setLang', lang)
  }
}

// mutations
const mutations = {
  setLang (state, lang) {
    app.$i18n.locale = lang
    state.lang = lang
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
