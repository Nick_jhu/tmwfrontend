import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  transferList: null,
  transfer: null,
  transferDetailList: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearTransferList ({ commit, state }, data) {
    commit('clearTransferList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.data)
      return response.data.data
    }).catch((error) => {
      console.log(error)
      return error.response.data
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.queryDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.transfer.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  deleteDetail ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.transfer.deleteDetail}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('deleteDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  confirm ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.transfer.confirm}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('confirm', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.transferList = null
    state.transfer = null
    state.transferDetailList = null
  },
  clearTransferList (state) {
    state.transferList = null
  },
  query (state, data) {
    state.transferList = data
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  deleteDetail (state, data) {
    // console.log('delete', data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.transfer = data.recTransfer || null
    state.transferDetailList = data.transferDetails || []
  },
  clearDetail (state, data) {
    state.transfer = null
    state.transferDetailList = null
  },
  dataexport (state, data) {
    // console.log(data)
  },
  confirm (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
