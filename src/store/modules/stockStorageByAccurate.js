import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  stockStorageByAccurateList: null,
  stockStorageByAccurate: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStockStorageByAccurateList ({ commit, state }, data) {
    commit('clearStockStorageByAccurateList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.query}${data.companyId}/${data.whId}/${data.ownerId}/${data.specifyDateTime}`
    return app.axios.post(api, data.postData, { timeout: 180000 }).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.stockAccurate)
      return response.data
    }).catch((error) => {
      // console.log(error)
      // console.log(error.response)
      return error.response.data
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.query}${data.companyId}/${data.whId}/${data.ownerId}/${data.specifyDateTime}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.stockAccurate)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.query}${data.companyId}/${data.whId}/${data.ownerId}/${data.specifyDateTime}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.stockAccurate)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.stockAccurateByDateByInventory)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockStorageByAccurate.dataexport}${data.companyId}/${data.whId}/${data.ownerId}/${data.specifyDateTime}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.stockStorageByAccurateList = null
    state.stockStorageByAccurate = null
  },
  clearStockStorageByAccurateList (state) {
    state.stockStorageByAccurateList = null
  },
  query (state, data) {
    // console.log(data)
    state.stockStorageByAccurateList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.stockStorageByAccurate = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.stockStorageByAccurate = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  dataexport (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
