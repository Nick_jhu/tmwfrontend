import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  simpleProcessingList: null,
  simpleProcessing: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearSimpleProcessingList ({ commit, state }, data) {
    commit('clearSimpleProcessingList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.simpleProcessing.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.transfer)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.simpleProcessing.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.transfer)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.simpleProcessing.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.transfer)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.simpleProcessing.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.transfer)
      return response.data
    }).catch((error) => {
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.simpleProcessingList = null
    state.simpleProcessing = null
  },
  clearSimpleProcessingList (state) {
    state.simpleProcessingList = null
  },
  query (state, data) {
    state.simpleProcessingList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.simpleProcessing = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.simpleProcessing = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
