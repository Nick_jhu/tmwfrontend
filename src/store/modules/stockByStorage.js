import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  stockByStorageList: null,
  stockByStorage: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStockByStorageList ({ commit, state }, data) {
    commit('clearStockByStorageList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockByStorage.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  byDateQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockByStorage.byDateQuery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('byDateQuery', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockByStorage.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockByStorage.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockByStorage.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  byDateDataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.stockByStorage.byDateDataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('byDateDataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.stockByStorageList = null
    state.stockByStorage = null
  },
  clearStockByStorageList (state) {
    state.stockByStorageList = null
  },
  query (state, data) {
    state.stockByStorageList = data
  },
  byDateQuery (state, data) {
    state.stockByStorageList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.stockByStorage = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.stockByStorage = null
  },
  dataexport (state) {
    // console.log(state)
  },
  byDateDataexport (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
