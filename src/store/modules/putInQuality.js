import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  putInQualityList: null,
  putInQuality: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPutInQualityList ({ commit, state }, data) {
    commit('clearPutInQualityList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInQuality.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.inbound)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInQuality.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.inbound)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInQuality.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.inbound)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInQuality.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.inbound)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInQuality.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.putInQuality.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.putInQualityList = null
    state.putInQuality = null
  },
  clearPutInQualityList (state) {
    state.putInQualityList = null
  },
  query (state, data) {
    state.putInQualityList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.putInQuality = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.putInQuality = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
