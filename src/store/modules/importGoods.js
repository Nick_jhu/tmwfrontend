import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  upload ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.importGoods.upload}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('upload', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  import ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.importGoods.import}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  importUnitconvert ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.importGoods.importUnitconvert}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  productRelationImport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.importGoods.productRelationImport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  upload (state) {
    // console.log(state)
  },
  import (state) {
    // console.log(state)
  },
  productRelationImport (state) {
    // console.log(state)
  },
  importUnitconvert (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
