import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  unitConvertList: null,
  unitConvert: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearUnitConvertList ({ commit, state }, data) {
    commit('clearUnitConvertList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.unitConvert.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.unitConvert)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.unitConvert.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.unitConvert)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.unitConvert.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.unitConvert)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.unitConvert.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.respondCode.additionals.unitConvert)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.unitConvert.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.unitConvert.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.unitConvert)
      return response.data
    }).catch((error) => {
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.unitConvertList = null
    state.unitConvert = null
  },
  clearUnitConvertList (state) {
    state.unitConvertList = null
  },
  query (state, data) {
    state.unitConvertList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.unitConvert = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.unitConvert = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
