import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  inventoryBookingList: null,
  inventoryBooking: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearInventoryBookingList ({ commit, state }, data) {
    commit('clearInventoryBookingList')
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inventoryBooking.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.productSn)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.inventoryBookingList = null
    state.inventoryBooking = null
  },
  clearInventoryBookingList (state) {
    state.inventoryBookingList = null
  },
  create (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
