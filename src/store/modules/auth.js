import { app } from '../../main'
import Config from '../../Config'
// import user from './user'

// initial state
const state = {
  auth: null,
  accessToken: null,
  refreshToken: null,
  userInfo: JSON.parse(window.localStorage.getItem('app_userInfo')) || null,
  menuTree: JSON.parse(window.localStorage.getItem('app_menuTree')) || null,
  menuState: JSON.parse(window.localStorage.getItem('app_menuState')) || {},
  permissions: JSON.parse(window.localStorage.getItem('app_permissions')) || null,
  functionSchema: JSON.parse(window.localStorage.getItem('app_functionSchema')) || null,
  winGridActive: 0,
  stageType: JSON.parse(window.localStorage.getItem('app_stageType')) || 'W'
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// const apiBaseUrl = `http://localhost:8082/API`
// const apiBaseUrl = `http://dev-wms.target-ai.com/API`

// 使用utf-8字符集解析base64字符串
// const atou = function (str) {
//   return decodeURIComponent(escape(window.atob(str)))
// }
// const parseJwt = function (token) {
//   var base64Url = token.split('.')[1]
//   var base64 = base64Url.replace('-', '+').replace('_', '/')
//   return JSON.parse(atou(base64))
// }

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }) {
    commit('clear')
  },
  logout ({ commit, state }, data) {
    // commit('logout', 'success')
    let api = `${apiBaseUrl}${Config.API.Auth.logout}`
    return app.axios.post(api).then((response) => {
      // console.log(response.data)
      commit('logout', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      commit('logout', error.response)
      return error.response
    })
  },
  async login ({ commit, state }, data) {
    console.log(data)
    let api = `${apiBaseUrl}${Config.API.Auth.login}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      let userRes = response.data
      if (userRes.success) {
        let accessToken = userRes.access_token
        let refreshToken = userRes.refresh_token
        app.axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`
        app.$localStorage.set('accessToken', `Bearer ${accessToken}`)
        app.$localStorage.set('refreshToken', `${refreshToken}`)
        let userData = userRes.userData
        // let warehouseData = userRes.warehouse1[0]
        //
        let res = {
          'message': null,
          'success': true,
          'data': {
            'stageType': 'W',
            'accessToken': accessToken,
            'refreshToken': refreshToken,
            'userInfo': {
              'avatar': 'https://i.loli.net/2017/08/21/599a521472424.jpg',
              'userName': userData.name,
              'nickname': userData.username,
              'emial': userData.email,
              'uid': userData.id,
              'userId': userData.id,
              'uiId': 1,
              'layoutMode': 'V',
              'themeColor': userRes.data.userInfo.themeColor,
              'phone': '1234567890',
              'companyLocalName': '測試',
              'companyEnglishName': 'bunny',
              'companyKey': userData.company_id,
              'warehousLocalName': '測試',
              'warehousEnglishName': 'bunny',
              'warehouseKey': userData.wh_id,
              'warehouseList': userRes.warehouse1,
              'useFixedOwnerId': null,
              'ownerKey': null,
              'ownerName': null,
              'ownerNo': null
            },
            'menuTree': userRes.data.menuTree,
            // 'menuTree': [
            //   {
            //     'id': 'home',
            //     'rootId': 'home',
            //     'icon': 'home',
            //     'nodes': []
            //   },
            //   {
            //     'id': 'basicMgmt',
            //     'rootId': null,
            //     'icon': 'address-book',
            //     'nodes': [
            //       {
            //         'id': 'basicParameterSetup',
            //         'rootId': 'basicMgmt',
            //         'nodes': []
            //       },
            //       {
            //         'id': 'customerSetup',
            //         'rootId': 'basicMgmt',
            //         'nodes': []
            //       },
            //       {
            //         'id': 'userSetup',
            //         'rootId': 'basicMgmt',
            //         'nodes': []
            //       },
            //       {
            //         'id': 'custuserSetup',
            //         'rootId': 'basicMgmt',
            //         'nodes': []
            //       },
            //       {
            //         'id': 'carSetup',
            //         'rootId': 'basicMgmt',
            //         'nodes': []
            //       },
            //       // {
            //       //   'id': 'settingMenuSetup',
            //       //   'rootId': 'basicMgmt',
            //       //   'nodes': []
            //       // },
            //       {
            //         'id': 'roleSetup',
            //         'rootId': 'basicMgmt',
            //         'nodes': []
            //       }
            //       // {
            //       //   'id': 'permissionSetup',
            //       //   'rootId': 'basicMgmt',
            //       //   'nodes': []
            //       // },
            //       // {
            //       //   'id': 'announceSetup',
            //       //   'rootId': 'basicMgmt',
            //       //   'nodes': []
            //       // }
            //       // {
            //       //   'id': 'settingCodeSetup',
            //       //   'rootId': 'basicMgmt',
            //       //   'nodes': []
            //       // }
            //     ]
            //   },
            //   {
            //     'id': 'orderMgmt',
            //     'rootId': null,
            //     'icon': 'box-open',
            //     'nodes': [
            //       {
            //         'id': 'orderSetup',
            //         'rootId': 'orderMgmt',
            //         'nodes': []
            //       }
            //     ]
            //   }
            //   // {
            //   //   'id': 'goodsMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'box-open',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'goodsSetup',
            //   //       'rootId': 'goodsMgmt',
            //   //       'nodes': []
            //   //     }
            //   //     // {
            //   //     //   'id': 'goodsCateSetup',
            //   //     //   'rootId': 'goodsMgmt',
            //   //     //   'nodes': []
            //   //     // }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'inventoryMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'chart-line',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'inventoryQueryByProd',
            //   //       'rootId': 'inventoryMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'inventoryQueryByStorage',
            //   //       'rootId': 'inventoryMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'inventoryQueryByinboundDate',
            //   //       'rootId': 'inventoryMgmt',
            //   //       'nodes': []
            //   //     }
            //   //     // {
            //   //     //   'id': 'inventoryQueryBySummary',
            //   //     //   'rootId': 'inventoryMgmt',
            //   //     //   'nodes': []
            //   //     // }
            //   //     // {
            //   //     //   'id': 'inventoryQueryAccurate',
            //   //     //   'rootId': 'inventoryMgmt',
            //   //     //   'nodes': []
            //   //     // }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'putInMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'people-carry',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'putInOperationSimple',
            //   //       'rootId': 'putInMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     // {
            //   //     //   'id': 'putInAcceptance',
            //   //     //   'rootId': 'putInMgmt',
            //   //     //   'nodes': []
            //   //     // },
            //   //     {
            //   //       'id': 'inventoryQuery',
            //   //       'rootId': 'putInMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'inboundDetailPutaway',
            //   //       'rootId': 'putInMgmt',
            //   //       'nodes': []
            //   //     }
            //   //     // {
            //   //     //   'id': 'inboundSnapshot',
            //   //     //   'rootId': 'putInMgmt',
            //   //     //   'nodes': []
            //   //     // }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'outboundMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'truck',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'outboundOperation',
            //   //       'rootId': 'outboundMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'bookingOperation',
            //   //       'rootId': 'outboundMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'pickingSetup',
            //   //       'rootId': 'outboundMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'pickingByConsigneeSetup',
            //   //       'rootId': 'outboundMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     // {
            //   //     //   'id': 'confirmMgmt',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': [
            //   //     //     {
            //   //     //       'id': 'pickingOperation',
            //   //     //       'rootId': 'outboundMgmt',
            //   //     //       'nodes': []
            //   //     //     },
            //   //     //     {
            //   //     //       'id': 'pickingConfirm',
            //   //     //       'rootId': 'outboundMgmt',
            //   //     //       'nodes': []
            //   //     //     }
            //   //     //   ]
            //   //     // },
            //   //     // {
            //   //     //   'id': 'pickingListQuery',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': []
            //   //     // },
            //   //     // {
            //   //     //   'id': 'boxingSetup',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': []
            //   //     // },
            //   //     // {
            //   //     //   'id': 'boxingMgmt',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': [
            //   //     //     {
            //   //     //       'id': 'fastBoxingSetup',
            //   //     //       'rootId': 'outboundMgmt',
            //   //     //       'nodes': []
            //   //     //     },
            //   //     //     {
            //   //     //       'id': 'boxingSetup',
            //   //     //       'rootId': 'outboundMgmt',
            //   //     //       'nodes': []
            //   //     //     },
            //   //     //     // {
            //   //     //     //   'id': 'boxingDetailsSetup',
            //   //     //     //   'rootId': 'outboundMgmt',
            //   //     //     //   'nodes': []
            //   //     //     // },
            //   //     //     {
            //   //     //       'id': 'boxDetailsQuery',
            //   //     //       'rootId': 'outboundMgmt',
            //   //     //       'nodes': []
            //   //     //     }
            //   //     //   ]
            //   //     // },
            //   //     // {
            //   //     //   'id': 'productSnQuery',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': []
            //   //     // },
            //   //     // {
            //   //     //   'id': 'deliveryConfirm',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': []
            //   //     // },
            //   //     {
            //   //       'id': 'outbondQualitySetup',
            //   //       'rootId': 'outboundMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'outboundDetailQuery',
            //   //       'rootId': 'outboundMgmt',
            //   //       'nodes': []
            //   //     }
            //   //     // {
            //   //     //   'id': 'outboundSnapshot',
            //   //     //   'rootId': 'outboundMgmt',
            //   //     //   'nodes': []
            //   //     // }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'warehouseMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'warehouse',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'storageSetup',
            //   //       'rootId': 'warehouseMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'storageChangeSetup',
            //   //       'rootId': 'warehouseMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'adjustmentInboundSetup',
            //   //       'rootId': 'warehouseMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'adjustmentOutboundSetup',
            //   //       'rootId': 'warehouseMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'transferSetup',
            //   //       'rootId': 'warehouseMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     // {
            //   //     //   'id': 'simpleProcessingSetup',
            //   //     //   'rootId': 'warehouseMgmt',
            //   //     //   'nodes': []
            //   //     // },
            //   //     {
            //   //       'id': 'inventoryAndOutboundDetailQuery',
            //   //       'rootId': 'warehouseMgmt',
            //   //       'nodes': []
            //   //     }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'returnMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'warehouse',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'returnOperationSimple',
            //   //       'rootId': 'returnMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'returnInventoryQuery',
            //   //       'rootId': 'returnMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'returnSnapshot',
            //   //       'rootId': 'returnMgmt',
            //   //       'nodes': []
            //   //     }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'stocktakeMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'boxes',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'stocktakeSetup',
            //   //       'rootId': 'stocktakeMgmt',
            //   //       'nodes': []
            //   //     }
            //   //   ]
            //   // }
            //   // {
            //   //   'id': 'quotationMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'file-invoice-dollar',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'quotationSetup',
            //   //       'rootId': 'quotationMgmt',
            //   //       'nodes': []
            //   //     },
            //   //     {
            //   //       'id': 'billSetup',
            //   //       'rootId': 'quotationMgmt',
            //   //       'nodes': []
            //   //     }
            //   //   ]
            //   // },
            //   // {
            //   //   'id': 'edihubMgmt',
            //   //   'rootId': null,
            //   //   'icon': 'file-export',
            //   //   'nodes': [
            //   //     {
            //   //       'id': 'exportSetup',
            //   //       'rootId': 'edihubMgmt',
            //   //       'nodes': []
            //   //     }
            //   //   ]
            //   // }
            //   // {
            //   //   'id' : 'demo',
            //   //   'rootId': null,
            //   //   'icon' : 'sliders-h',
            //   //   'nodes' : [
            //   //     {
            //   //       'id' : 'demoItem1',
            //   //       'rootId': 'demo',
            //   //       'nodes' : []
            //   //     },
            //   //     {
            //   //       'id' : 'demoItem2',
            //   //       'rootId': 'demo',
            //   //       'nodes' : []
            //   //     },
            //   //     {
            //   //       'id' : 'demoItem3',
            //   //       'rootId': 'demo',
            //   //       'nodes' : []
            //   //     },
            //   //     {
            //   //       'id' : 'demoItem4',
            //   //       'rootId': 'demo',
            //   //       'nodes' : []
            //   //     },
            //   //     {
            //   //       'id' : 'demoItem5',
            //   //       'rootId': 'demo',
            //   //       'nodes' : [
            //   //         {
            //   //           'id' : 'demoItem5-1',
            //   //           'rootId': 'demo',
            //   //           'nodes' : []
            //   //         }
            //   //       ]
            //   //     }
            //   //   ]
            //   // }
            // ],
            'permissions': {
              'goodsSetup': {
                'rootId': 'goodsMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true,
                  'dataexport': false
                }
              },
              'goodsCateSetup': {
                'rootId': 'goodsMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'countrySetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'citySetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'distSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'stateProvinceSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'warehouseSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'basicParameterSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'orderSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'dlvplanSetup': {
                'rootId': 'dlvplanMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'dlvmissionSetup': {
                'rootId': 'dlvplanMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'customerSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'contactSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'userSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'custuserSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'carSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'settingMenuSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'roleSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'permissionSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'settingfeeSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'settingCodeSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'announceSetup': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'currencyCode': {
                'rootId': 'basicMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'fastBoxingSetup': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'boxingDetailsSetup': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'boxDetailsQuery': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'storageSetup': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'storageChangeSetup': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'adjustmentInboundSetup': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'adjustmentOutboundSetup': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'transferSetup': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'simpleProcessingSetup': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'inventoryAndOutboundDetailQuery': {
                'rootId': 'warehouseMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'putInOperationSimple': {
                'rootId': 'putInMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'putInAcceptance': {
                'rootId': 'putInMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'inventoryQuery': {
                'rootId': 'putInMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'inboundDetailPutaway': {
                'rootId': 'putInMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'inboundSnapshot': {
                'rootId': 'putInMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'outboundOperation': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'bookingOperation': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'pickingSetup': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'pickingByConsigneeSetup': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'pickingOperation': {
                'rootId': 'confirmMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'pickingListQuery': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'productSnQuery': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'outbondQualitySetup': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'outboundDetailQuery': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'outboundSnapshot': {
                'rootId': 'outboundMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'returnOperationSimple': {
                'rootId': 'returnMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'returnOperation': {
                'rootId': 'returnMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'returnAcceptance': {
                'rootId': 'returnMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'returnShelves': {
                'rootId': 'returnMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'returnInventoryQuery': {
                'rootId': 'returnMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'returnSnapshot': {
                'rootId': 'returnMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'stocktakeSetup': {
                'rootId': 'stocktakeMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'exportSetup': {
                'rootId': 'edihubMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'quotationSetup': {
                'rootId': 'quotationMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              },
              'billSetup': {
                'rootId': 'quotationMgmt',
                'permission': {
                  'create': true,
                  'update': true,
                  'query': true,
                  'delete': true,
                  'copy': true
                }
              }
            },
            'functionSchema': userRes.data.functionSchema
            // 'functionSchema': {
            //   'goodsSetup': {
            //     'rootId': 'goodsMgmt',
            //     'schema': [
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode',
            //         'displayName': '商品條碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode2',
            //         'displayName': '商品條碼 2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_spec',
            //         'displayName': '商品規格',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_length',
            //         'displayName': '長',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_width',
            //         'displayName': '寬',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_height',
            //         'displayName': '高',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'length_unit_name',
            //         'displayName': '長度單位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'expiration_days',
            //         'displayName': '保存天數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'box_in_num',
            //         'displayName': '箱入數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pallet_in_num',
            //         'displayName': '板入數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pallet_placement',
            //         'displayName': '擺放方式',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_unitprice',
            //         'displayName': '商品售價',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_cost_price',
            //         'displayName': '商品成本',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_brand',
            //         'displayName': '商品品牌',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'need_scan_product_sn',
            //         'displayName': '回刷序號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_note',
            //         'displayName': '商品備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'goodsCateSetup': {
            //     'rootId': 'goodsMgmt',
            //     'schema': [
            //       {
            //         'property': 'pc_root_name',
            //         'displayName': '上一層商品類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pc_sort',
            //         'displayName': '商品類別預設排序',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pc_code',
            //         'displayName': '商品類別代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pc_name',
            //         'displayName': '商品類別名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'countrySetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'region_name',
            //         'displayName': '區域',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_code',
            //         'displayName': '國別代碼(兩碼)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_code_alpha3',
            //         'displayName': '國別代碼(三碼)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '國別名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'stateSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'province_code',
            //         'displayName': '州省郡代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'province_name',
            //         'displayName': '主要州省郡名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '國家',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'citySetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'city_code',
            //         'displayName': '城市代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'city_name',
            //         'displayName': '城市名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '國家',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'province_name',
            //         'displayName': '州省郡',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'districtSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'postal_code',
            //         'displayName': '郵遞區號',
            //         'type': 'Number'
            //       },
            //       {
            //         'property': 'district_name',
            //         'displayName': '行政區名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '主要國家名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'province_name',
            //         'displayName': '主要州省郡名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'city_name',
            //         'displayName': '城市名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'warehouseSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'wh_no',
            //         'displayName': '倉庫代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'wh_name',
            //         'displayName': '倉庫名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'wh_name_eng',
            //         'displayName': '倉庫名稱(次)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'telephone',
            //         'displayName': '電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'fax',
            //         'displayName': '傳真',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'address',
            //         'displayName': '地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'customerSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'customer_no',
            //         'displayName': '客戶編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_cat_name',
            //         'displayName': '客戶類型',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_name',
            //         'displayName': '客戶名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vatno',
            //         'displayName': '客戶統一編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_days_allowed_rule',
            //         'displayName': '允售條件',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'address',
            //         'displayName': '客戶地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'address_invoice',
            //         'displayName': '客戶發票地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'telephone',
            //         'displayName': '客戶電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'fax',
            //         'displayName': '客戶傳真',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'website',
            //         'displayName': '客戶網址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_firstname',
            //         'displayName': '聯絡人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_note',
            //         'displayName': '客戶備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'customer': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'wh_name',
            //         'displayName': '倉庫',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_no',
            //         'displayName': '客戶編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_name',
            //         'displayName': '客戶名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vatno',
            //         'displayName': '客戶統一編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'address',
            //         'displayName': '客戶地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'address_invoice',
            //         'displayName': '客戶發票地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'telephone',
            //         'displayName': '客戶電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'fax',
            //         'displayName': '客戶傳真',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'website',
            //         'displayName': '客戶網址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_firstname',
            //         'displayName': '聯絡人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_note',
            //         'displayName': '客戶備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'basicParameterSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'cd',
            //         'displayName': '代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'cd_descp',
            //         'displayName': '名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'value1',
            //         'displayName': '參數1',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'value2',
            //         'displayName': '參數2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'value3',
            //         'displayName': '參數3',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '修改人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'orderSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'sys_ord_no',
            //         'displayName': '系統訂單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'cust_ord_no',
            //         'displayName': '客戶訂單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'use_type_name',
            //         'displayName': '運輸類型名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'status_desc',
            //         'displayName': '訂單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'booking_no',
            //         'displayName': '追蹤號碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'use_date',
            //         'displayName': '需求日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'so_no',
            //         'displayName': 'S/O NO',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bl_no',
            //         'displayName': '提單號碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ship_comp',
            //         'displayName': '船公司',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ship_name',
            //         'displayName': '船名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ship_id',
            //         'displayName': '航次',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickup_loc',
            //         'displayName': '領櫃地',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'redeliv',
            //         'displayName': '交櫃地',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_off_plant',
            //         'displayName': '卸櫃廠別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'on_con_plant',
            //         'displayName': '裝櫃廠別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_type',
            //         'displayName': '櫃型',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_id1',
            //         'displayName': '櫃號1',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_id2',
            //         'displayName': '櫃號2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'import_forward',
            //         'displayName': '進口貨代',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'import_forward_comp',
            //         'displayName': '進口報關行',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'export_forward',
            //         'displayName': '出口貨代',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'export_forward_comp',
            //         'displayName': '出口報關行',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'truck_id',
            //         'displayName': '拖車公司',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'on_off_con_date',
            //         'displayName': '裝/退櫃日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'disable',
            //         'displayName': '取消',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'remark',
            //         'displayName': 'EIR註記',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pol',
            //         'displayName': '裝貨港',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pod',
            //         'displayName': '卸貨港',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inx_owner',
            //         'displayName': '群創派遣人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '修改人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'order': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'sys_ord_no',
            //         'displayName': '系統訂單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'cust_ord_no',
            //         'displayName': '客戶訂單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'use_type_name',
            //         'displayName': '運輸類型名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'status_desc',
            //         'displayName': '訂單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'booking_no',
            //         'displayName': '追蹤號碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'use_date',
            //         'displayName': '需求日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'so_no',
            //         'displayName': 'S/O NO',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bl_no',
            //         'displayName': '提單號碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ship_comp',
            //         'displayName': '船公司',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ship_name',
            //         'displayName': '船名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ship_id',
            //         'displayName': '航次',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickup_loc',
            //         'displayName': '領櫃地',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'redeliv',
            //         'displayName': '交櫃地',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_off_plant',
            //         'displayName': '卸櫃廠別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'on_con_plant',
            //         'displayName': '裝櫃廠別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_type',
            //         'displayName': '櫃型',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_id1',
            //         'displayName': '櫃號1',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'con_id2',
            //         'displayName': '櫃號2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'import_forward',
            //         'displayName': '進口貨代',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'import_forward_comp',
            //         'displayName': '進口報關行',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'export_forward',
            //         'displayName': '出口貨代',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'export_forward_comp',
            //         'displayName': '出口報關行',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'truck_id',
            //         'displayName': '拖車公司',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'on_off_con_date',
            //         'displayName': '裝/退櫃日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'disable',
            //         'displayName': '取消',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'remark',
            //         'displayName': 'EIR註記',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pol',
            //         'displayName': '裝貨港',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pod',
            //         'displayName': '卸貨港',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inx_owner',
            //         'displayName': '群創派遣人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '修改人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'contactSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'contact_firstname',
            //         'displayName': '聯絡人(主)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_lastname',
            //         'displayName': '聯絡人(次)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_title',
            //         'displayName': '聯絡人(角色)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_phone',
            //         'displayName': '聯絡室內電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_mobile',
            //         'displayName': '聯絡行動電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_email',
            //         'displayName': '聯絡Email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'region_name',
            //         'displayName': '區域',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '國家',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'province_name',
            //         'displayName': '州省郡',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'city_name',
            //         'displayName': '城市',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'district_name',
            //         'displayName': '行政區',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_note',
            //         'displayName': '聯絡人備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_email',
            //         'displayName': '聯絡Email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'userSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'name',
            //         'displayName': '使用者名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'username',
            //         'displayName': '帳號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'email',
            //         'displayName': 'Email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'custuser': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'name',
            //         'displayName': '使用者名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'username',
            //         'displayName': '帳號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'email',
            //         'displayName': 'Email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'custuserSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'name',
            //         'displayName': '使用者名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'belong_station',
            //         'displayName': '所屬單位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'phone',
            //         'displayName': '電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'check_owner',
            //         'displayName': '可查看貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'role',
            //         'displayName': '角色',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'username',
            //         'displayName': '帳號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'email',
            //         'displayName': '電子郵件',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'carSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'belong_company',
            //         'displayName': '歸屬公司',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'belong_station',
            //         'displayName': '歸屬單位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'car_no',
            //         'displayName': '車牌號碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'licensing_date',
            //         'displayName': '領牌日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'license_expiry_date',
            //         'displayName': '行照到期日',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'status',
            //         'displayName': '車輛狀態代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'status_name',
            //         'displayName': '車輛狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'manufacture_date',
            //         'displayName': '出廠日期',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'property_number',
            //         'displayName': '財產編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'settingMenuSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'root_name',
            //         'displayName': '上一層選單',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_item',
            //         'displayName': '是否為功能',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'menu_sort',
            //         'displayName': '選單預設排序',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'menu_code',
            //         'displayName': '選單代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'menu_name',
            //         'displayName': '選單名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'menu_path',
            //         'displayName': '選單功能網址路徑',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'roleSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'name',
            //         'displayName': '角色代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'display_name',
            //         'displayName': '角色名稱',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'permissionSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'menu_name',
            //         'displayName': '選單',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'permission_code',
            //         'displayName': '權限項目代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'permission_name',
            //         'displayName': '權限項目名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'settingfeeSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'fee_code',
            //         'displayName': '費用項目代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'fee_name',
            //         'displayName': '費用名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'settingCodeSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'code_code',
            //         'displayName': '代碼代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'code_name',
            //         'displayName': '代碼名稱',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'announceSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'wh_name',
            //         'displayName': '倉庫',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'announce_type',
            //         'displayName': '公告型態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'announce_date',
            //         'displayName': '公告日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'announce_start_date',
            //         'displayName': '開始日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'announce_end_date',
            //         'displayName': '結束日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'announce_subject',
            //         'displayName': '主旨',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'companySetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'company_id',
            //         'displayName': '公司ID',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'company_name',
            //         'displayName': '公司名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vatno',
            //         'displayName': '統編',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'settingUnitSetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'unit_type',
            //         'displayName': '單位類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'unit_code',
            //         'displayName': '單位代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'unit_name',
            //         'displayName': '單位名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_enable',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'currencyCode': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'currency_code',
            //         'displayName': '幣別代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'currency_name',
            //         'displayName': '幣別名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_code',
            //         'displayName': '國別代碼(兩碼)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_code_alpha3',
            //         'displayName': '國別代碼(三碼)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '國別名稱',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'warehouse': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'wh_id',
            //         'displayName': '倉庫ID',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'group_name',
            //         'displayName': '集團',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'company_name',
            //         'displayName': '公司',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'wh_name',
            //         'displayName': '倉庫名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'wh_no',
            //         'displayName': '倉庫編號/代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'region_name',
            //         'displayName': '區域',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'country_name',
            //         'displayName': '國家',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'province_name',
            //         'displayName': '州省郡',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'city_name',
            //         'displayName': '城市',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'district_name',
            //         'displayName': '行政區',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'address',
            //         'displayName': '倉庫地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'telephone',
            //         'displayName': '倉庫電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'fax',
            //         'displayName': '倉庫傳真',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'website',
            //         'displayName': '倉庫網址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'region': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'region_id',
            //         'displayName': '區域ID',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'region_code',
            //         'displayName': '區域代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'region_name',
            //         'displayName': '區域名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'customerCategorySetup': {
            //     'rootId': 'basicMgmt',
            //     'schema': [
            //       {
            //         'property': 'customer_name',
            //         'displayName': '客戶名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_cat',
            //         'displayName': '類型代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'customer_cat_name',
            //         'displayName': '類別名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'isEnabled',
            //         'displayName': '已啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'storageSetup': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'storage_code',
            //         'displayName': '儲位編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'building_no',
            //         'displayName': '棟/倉別號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'floor_no',
            //         'displayName': '樓層號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'area1_no',
            //         'displayName': '儲區號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'lane_no',
            //         'displayName': '排/走道號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'rack_no',
            //         'displayName': '貨架號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'level_no',
            //         'displayName': '層號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_no',
            //         'displayName': '儲格/位號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_enabled',
            //         'displayName': '啟用',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'transferSetup': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'transfer_no',
            //         'displayName': '調撥單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'transfer_status',
            //         'displayName': '調撥狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'wh_name',
            //         'displayName': '來源倉庫',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '來源貨主代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '來源貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_wh_name',
            //         'displayName': '目的倉庫',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_owner_no',
            //         'displayName': '目的貨主代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_owner_name',
            //         'displayName': '目的貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'transfer_note',
            //         'displayName': '調撥備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '調撥人員',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'simpleProcessingSetup': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'transfer_no',
            //         'displayName': '簡易加工單號',
            //         'type': 'String'
            //       },
            //       // {
            //       //   'property': 'order_type_name',
            //       //   'displayName' : '作業類別',
            //       //   'type': 'String'
            //       // },
            //       // {
            //       //   'property': 'from_wh_name',
            //       //   'displayName' : '來源倉庫',
            //       //   'type': 'String'
            //       // },
            //       {
            //         'property': 'from_owner_name',
            //         'displayName': '來源貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'from_product_code',
            //         'displayName': '來源商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'from_product_name',
            //         'displayName': '來源商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'from_stock_num',
            //         'displayName': '來源數量(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'from_stock_num_bad',
            //         'displayName': '來源數量(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'from_stock_unit_name',
            //         'displayName': '來源單位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_product_code',
            //         'displayName': '目的商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_product_name',
            //         'displayName': '目的商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_stock_loc_code',
            //         'displayName': '目的儲位(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_stock_loc_bad_code',
            //         'displayName': '目的儲位(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_stock_num',
            //         'displayName': '目的數量(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_stock_num_bad',
            //         'displayName': '目的數量(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_stock_unit_name',
            //         'displayName': '目的單位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_product_batch_no',
            //         'displayName': '目的批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'to_product_expired_date',
            //         'displayName': '目的有效日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'to_inventory_note',
            //         'displayName': '目的庫存紀錄備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '加工人員',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'storageChangeSetup': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'created_at',
            //         'displayName': '移倉日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主代碼',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主名稱',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'before_storage_code',
            //         'displayName': '原儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'before_stock_num',
            //         'displayName': '原庫存',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'after_storage_code',
            //         'displayName': '新儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'after_stock_num',
            //         'displayName': '新庫存',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'adjustmentInboundSetup': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_status',
            //         'displayName': '調整狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '調整單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'adjustmentOutboundSetup': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'outbound_status',
            //         'displayName': '調整狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '調整單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note',
            //         'displayName': '備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inventoryAndOutboundDetailQuery': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_bill_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_type_code_name',
            //         'displayName': '作業類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_code',
            //         'displayName': '儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_quality',
            //         'displayName': '良品/不良品',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_num',
            //         'displayName': '數量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '有效日期',
            //         'type': 'Date'
            //       },
            //       // {
            //       //   'property': 'product_spec',
            //       //   'displayName': '商品規格',
            //       //   'type': 'String'
            //       // },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_note',
            //         'displayName': '備註',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inventory': {
            //     'rootId': 'warehouseMgmt',
            //     'schema': [
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'stock_num',
            //         'displayName': '已上架量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_preassign_storage_code',
            //         'displayName': '參考儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_storage_code',
            //         'displayName': '儲位(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_bad_storage_code',
            //         'displayName': '儲位(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_note',
            //         'displayName': '備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '有效日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'inventoryQueryByProd': {
            //     'rootId': 'inventoryMgmt',
            //     'schema': [
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主/供應商代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode',
            //         'displayName': '條碼1',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode2',
            //         'displayName': '條碼2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_quality',
            //         'displayName': '良品/不良品',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'actual_nums',
            //         'displayName': '總庫存',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'booked_nums',
            //         'displayName': '已分配量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'available_nums',
            //         'displayName': '可用量',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inventoryQueryByStorage': {
            //     'rootId': 'inventoryMgmt',
            //     'schema': [
            //       {
            //         'property': 'storage_code',
            //         'displayName': '上架儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主/供應商代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode',
            //         'displayName': '條碼1',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode2',
            //         'displayName': '條碼2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_quality',
            //         'displayName': '良品/不良品',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'actual_nums',
            //         'displayName': '總庫存',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'booked_nums',
            //         'displayName': '已分配量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'available_nums',
            //         'displayName': '可用量',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inventoryQueryByinboundDate': {
            //     'rootId': 'inventoryMgmt',
            //     'schema': [
            //       {
            //         'property': 'storage_code',
            //         'displayName': '儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主/供應商代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode',
            //         'displayName': '商品條碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode2',
            //         'displayName': '商品條碼2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_days',
            //         'displayName': '存放天數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_put_away_confirm',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_quality',
            //         'displayName': '良品/不良品',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'actual_nums',
            //         'displayName': '總庫存',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'booked_nums',
            //         'displayName': '已分配量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'available_nums',
            //         'displayName': '可用量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '有效日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'box_no',
            //         'displayName': '箱號',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inventoryQueryBySummary': {
            //     'rootId': 'inventoryMgmt',
            //     'schema': [
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'query_daytime',
            //         'displayName': '庫存日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'total_actual_nums',
            //         'displayName': '總庫存',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_available_nums',
            //         'displayName': '可用庫存',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_storage_nums',
            //         'displayName': '總儲位數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_inbound_nums',
            //         'displayName': '入庫數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_inbound_pallet_nums',
            //         'displayName': '入庫板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_inbound_box_nums',
            //         'displayName': '入庫箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_outbound_nums',
            //         'displayName': '出庫數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_outbound_pallet_nums',
            //         'displayName': '出庫板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_outbound_box_nums',
            //         'displayName': '出庫箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_outbound_box_nums_converted',
            //         'displayName': '出庫單實際出庫總數箱數',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inventoryQueryAccurate': {
            //     'rootId': 'inventoryMgmt',
            //     'schema': [
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode',
            //         'displayName': '商品條碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_barcode2',
            //         'displayName': '商品條碼2',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'stock_num',
            //         'displayName': '庫存(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'stock_num_bad',
            //         'displayName': '庫存(不良品)',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'putInOperationSimple': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '入庫單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_status',
            //         'displayName': '入庫單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主/供應商代號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_bill_no',
            //         'displayName': '採購單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '預計入庫日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_type',
            //         'displayName': '入庫類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_pallets',
            //         'displayName': '板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_boxes',
            //         'displayName': '箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_quantity_check',
            //         'displayName': '驗收日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '入庫單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_inbound',
            //         'displayName': '入庫通知時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_cancel',
            //         'displayName': '入庫取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_quality_check',
            //         'displayName': '品檢確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away',
            //         'displayName': '上架時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away_confirm_display',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'inventoryQuery': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '入庫系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_status',
            //         'displayName': '入庫單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_status',
            //         'displayName': '入庫明細狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主/供應商編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_bill_no',
            //         'displayName': '採購單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'box_no',
            //         'displayName': '箱號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_put_away_confirm_display',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_quality',
            //         'displayName': '良品/不良品',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_num',
            //         'displayName': '數量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_code',
            //         'displayName': '儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_note',
            //         'displayName': '明細備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '效期',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_inbound',
            //         'displayName': '入庫通知時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'inboundDetailPutaway': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '入庫系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_status',
            //         'displayName': '入庫單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_status',
            //         'displayName': '入庫明細狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'box_no',
            //         'displayName': '箱號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_quality',
            //         'displayName': '良品/不良品',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_code',
            //         'displayName': '儲位',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_num',
            //         'displayName': '數量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_put_away_confirm_display',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'inboundSnapshot': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '入庫單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_status',
            //         'displayName': '入庫單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_bill_no',
            //         'displayName': '進倉單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_bill_no',
            //         'displayName': '採購單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '預計入庫日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_type',
            //         'displayName': '入庫類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_pallets',
            //         'displayName': '板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_boxes',
            //         'displayName': '箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_quantity_check',
            //         'displayName': '驗收日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '入庫單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_inbound',
            //         'displayName': '入庫通知時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_cancel',
            //         'displayName': '入庫取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_quality_check',
            //         'displayName': '品檢確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away',
            //         'displayName': '上架時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'outboundOperation': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_status',
            //         'displayName': '出庫狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'operation_type',
            //         'displayName': '訂單類型',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_type',
            //         'displayName': '出庫類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickinglist_no',
            //         'displayName': '揀貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickinglist_no_byproduct',
            //         'displayName': '彙總揀貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'shipping_company_name',
            //         'displayName': '貨運行別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'temperature_type_name',
            //         'displayName': '運送溫層類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'shipping_no',
            //         'displayName': '託運單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_type',
            //         'displayName': '庫別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'store_code',
            //         'displayName': '門市代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickup_store',
            //         'displayName': '取貨門市',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickup_tag',
            //         'displayName': '取貨標籤',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'store_atd',
            //         'displayName': '門市進貨日',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'store_rtd',
            //         'displayName': '門市退貨日',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_no',
            //         'displayName': '配送編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_times',
            //         'displayName': '配送次數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'out_of_stock_dealing_method_name',
            //         'displayName': '缺貨處理方式',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note',
            //         'displayName': '出庫單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note2',
            //         'displayName': '出庫單第二備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note3',
            //         'displayName': '出庫單第三備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_weight',
            //         'displayName': '出庫總重量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_volume',
            //         'displayName': '出庫總材積',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_pallets',
            //         'displayName': '板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_boxes',
            //         'displayName': '箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_status',
            //         'displayName': '配送狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'take_number_note',
            //         'displayName': '取號備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_name',
            //         'displayName': '訂貨人姓名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_vatno',
            //         'displayName': '訂貨人統編',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_tel',
            //         'displayName': '訂貨人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_email',
            //         'displayName': '訂貨人email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_name',
            //         'displayName': '收件人姓名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_vatno',
            //         'displayName': '收件人統編',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_address',
            //         'displayName': '收件人地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_tel',
            //         'displayName': '收件人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_email',
            //         'displayName': '收件人email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_note',
            //         'displayName': '收件人備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ecstore_code',
            //         'displayName': '電商平台',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'dealer_name',
            //         'displayName': '經銷商名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_date_specify',
            //         'displayName': '指定配送日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'delivery_time_specify',
            //         'displayName': '指定配送時間',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_time_period_name',
            //         'displayName': '指定配送時段',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_date',
            //         'displayName': '訂單時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_outbound',
            //         'displayName': '出庫通知',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_close_order',
            //         'displayName': '截單時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_picked',
            //         'displayName': '揀貨時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_shipped',
            //         'displayName': '裝箱時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_closed',
            //         'displayName': '出貨時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'pickingListQuery': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_code',
            //         'displayName': '儲位(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_bad_code',
            //         'displayName': '儲位(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_unit_min_nums',
            //         'displayName': '數量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_name',
            //         'displayName': '收件人名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '有效日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'obd_note',
            //         'displayName': '出庫明細備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_lottable1',
            //         'displayName': '屬性1',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_lottable2',
            //         'displayName': '屬性2',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'fastBoxingSetup': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outboundStatus',
            //         'displayName': '出庫狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_name',
            //         'displayName': '收件人姓名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_vatno',
            //         'displayName': '收件人統編',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_city_name',
            //         'displayName': '收件人城市',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_district_name',
            //         'displayName': '收件人行政區',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_address',
            //         'displayName': '收件人地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_tel',
            //         'displayName': '收件人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_email',
            //         'displayName': '收件人email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_note',
            //         'displayName': '收件人備註',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'boxDetailsQuery': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'box_no',
            //         'displayName': '箱號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'boxed_num',
            //         'displayName': '數量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主代碼',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'productSnQuery': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '入庫單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'box_no',
            //         'displayName': '箱號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_sn',
            //         'displayName': '商品序號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'is_outbounded',
            //         'displayName': '是否已出貨',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_outbounded',
            //         'displayName': '裝箱時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'outboundDetailQuery': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_status',
            //         'displayName': '出庫狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_type',
            //         'displayName': '訂單類型',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_no',
            //         'displayName': '貨主代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note',
            //         'displayName': '出庫單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'store_code',
            //         'displayName': '門市代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_name',
            //         'displayName': '收件人名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_tel',
            //         'displayName': '收件人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_address',
            //         'displayName': '收件人地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_nums',
            //         'displayName': '訂購量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_nums_min',
            //         'displayName': '實際出貨量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'obd_note',
            //         'displayName': '明細備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_spec',
            //         'displayName': '商品規格',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '有效日期',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_closed',
            //         'displayName': '出貨時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_cancel',
            //         'displayName': '取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'obd_note_close',
            //         'displayName': '截單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'outboundSnapshot': {
            //     'rootId': 'outboundMgmt',
            //     'schema': [
            //       {
            //         'property': 'outbound_no',
            //         'displayName': '出庫單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_status',
            //         'displayName': '出庫狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'order_no',
            //         'displayName': '訂單編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'operation_type',
            //         'displayName': '訂單類型',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickinglist_no',
            //         'displayName': '揀貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'pickinglist_no_byproduct',
            //         'displayName': '彙總揀貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'shipping_company_name',
            //         'displayName': '貨運行別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'temperature_type_name',
            //         'displayName': '運送溫層類別',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'shipping_no',
            //         'displayName': '託運單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'store_code',
            //         'displayName': '門市代碼',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_no',
            //         'displayName': '配送編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_times',
            //         'displayName': '配送次數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'out_of_stock_dealing_method_name',
            //         'displayName': '缺貨處理方式',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note',
            //         'displayName': '出庫單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note2',
            //         'displayName': '出庫單第二備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_note3',
            //         'displayName': '出庫單第三備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_weight',
            //         'displayName': '出庫總重量',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'total_volume',
            //         'displayName': '出庫總材積',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_pallets',
            //         'displayName': '板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'outbound_boxes',
            //         'displayName': '箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_status',
            //         'displayName': '配送狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'take_number_note',
            //         'displayName': '取號備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_name',
            //         'displayName': '訂貨人姓名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_vatno',
            //         'displayName': '訂貨人統編',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_tel',
            //         'displayName': '訂貨人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'purchaser_email',
            //         'displayName': '訂貨人email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_name',
            //         'displayName': '收件人姓名',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_vatno',
            //         'displayName': '收件人統編',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_address',
            //         'displayName': '收件人地址',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_tel',
            //         'displayName': '收件人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_email',
            //         'displayName': '收件人email',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'consignee_note',
            //         'displayName': '收件人備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'dealer_name',
            //         'displayName': '經銷商名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_date_specify',
            //         'displayName': '指定配送日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'delivery_time_specify',
            //         'displayName': '指定配送時間',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'delivery_time_period_name',
            //         'displayName': '指定配送時段',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_outbound',
            //         'displayName': '出庫通知',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_close_order',
            //         'displayName': '截單時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_picked',
            //         'displayName': '揀貨時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_shipped',
            //         'displayName': '裝箱時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_closed',
            //         'displayName': '出貨時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'returnOperationSimple': {
            //     'rootId': 'returnMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '退貨編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_status',
            //         'displayName': '狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_bill_no',
            //         'displayName': '進倉單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_bill_no',
            //         'displayName': '採購單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vendor_name',
            //         'displayName': '退貨人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '退貨日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_pallets',
            //         'displayName': '板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_boxes',
            //         'displayName': '箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '退貨單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_inbound',
            //         'displayName': '入庫通知時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_quantity_check',
            //         'displayName': '驗收日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_cancel',
            //         'displayName': '退貨取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_quality_check',
            //         'displayName': '品檢確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away',
            //         'displayName': '上架時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away_confirm_display',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'returnOperation': {
            //     'rootId': 'returnMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '退貨編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_bill_no',
            //         'displayName': '出貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inboundStatus',
            //         'displayName': '狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vendor_name',
            //         'displayName': '退貨人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '退貨日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '退貨單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'returnAcceptance': {
            //     'rootId': 'returnMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '退貨編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_bill_no',
            //         'displayName': '出貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inboundStatus',
            //         'displayName': '狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vendor_name',
            //         'displayName': '退貨人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '退貨日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '退貨單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'returnShelves': {
            //     'rootId': 'returnMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '退貨編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_bill_no',
            //         'displayName': '出貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inboundStatus',
            //         'displayName': '狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vendor_name',
            //         'displayName': '退貨人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '退貨日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '退貨單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'returnInventoryQuery': {
            //     'rootId': 'putInMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '退貨系統單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inboundStatus',
            //         'displayName': '退貨單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_bill_no',
            //         'displayName': '採購單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主/供應商',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_inbound',
            //         'displayName': '退貨入庫時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'product_code',
            //         'displayName': '商品料號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_name',
            //         'displayName': '商品名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_num',
            //         'displayName': '數量(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_num_bad',
            //         'displayName': '數量(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_storage_code',
            //         'displayName': '儲位(良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'storage_loc_bad_storage_code',
            //         'displayName': '儲位(不良品)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '退貨備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inventory_note',
            //         'displayName': '明細備註',
            //         'type': 'String'
            //       },
            //       // {
            //       //   'property': 'product_spec',
            //       //   'displayName': '商品規格',
            //       //   'type': 'String'
            //       // },
            //       {
            //         'property': 'product_batch_no',
            //         'displayName': '批號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_expired_date',
            //         'displayName': '效期',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'product_manufactured',
            //         'displayName': '製造日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away_confirm_display',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       }
            //     ]
            //   },
            //   'returnSnapshot': {
            //     'rootId': 'returnMgmt',
            //     'schema': [
            //       {
            //         'property': 'inbound_no',
            //         'displayName': '退貨編號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_bill_no',
            //         'displayName': '出貨單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inboundStatus',
            //         'displayName': '狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'vendor_name',
            //         'displayName': '退貨人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_expect',
            //         'displayName': '退貨日期',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'inbound_note',
            //         'displayName': '退貨單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_pallets',
            //         'displayName': '板數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'inbound_boxes',
            //         'displayName': '箱數',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_inbound',
            //         'displayName': '入庫通知時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_cancel',
            //         'displayName': '入庫取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_quantity_check',
            //         'displayName': '驗收日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_quality_check',
            //         'displayName': '品檢確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away',
            //         'displayName': '上架時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_put_away_confirm_display',
            //         'displayName': '上架確認時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_by',
            //         'displayName': '建立人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_by',
            //         'displayName': '更新人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'stocktakeSetup': {
            //     'rootId': 'stocktakeMgmt',
            //     'schema': [
            //       {
            //         'property': 'stocktake_no',
            //         'displayName': '盤點單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'wh_name',
            //         'displayName': '倉庫',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'stocktake_status',
            //         'displayName': '盤點狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'change_start_date',
            //         'displayName': '異動起日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'change_end_date',
            //         'displayName': '異動迄日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_stocktake_built',
            //         'displayName': '盤點單建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_canceled',
            //         'displayName': '盤點單取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_stocktake_started',
            //         'displayName': '啟動盤點時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'quotationSetup': {
            //     'rootId': 'quotationMgmt',
            //     'schema': [
            //       {
            //         'property': 'quotation_no',
            //         'displayName': '報價單單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'quotation_customize_no',
            //         'displayName': '報價單自編單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'quotation_status',
            //         'displayName': '報價單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'currency_name',
            //         'displayName': '貨幣',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_start',
            //         'displayName': '報價單生效時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_end',
            //         'displayName': '報價單到期時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'can_extend',
            //         'displayName': '是否可延用報價',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'quotation_note',
            //         'displayName': '報價單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'quotation_note2',
            //         'displayName': '報價單第二備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'billSetup': {
            //     'rootId': 'quotationMgmt',
            //     'schema': [
            //       {
            //         'property': 'bill_no',
            //         'displayName': '帳單單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bill_customize_no',
            //         'displayName': '帳單自編單號',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'owner_name',
            //         'displayName': '貨主',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bill_status',
            //         'displayName': '帳單狀態',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bill_price_untax',
            //         'displayName': '請款總金額(未稅)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'tax_rate',
            //         'displayName': '稅率',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bill_price_taxed',
            //         'displayName': '請款總金額(含稅)',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'currency_name',
            //         'displayName': '貨幣',
            //         'type': 'String'
            //       },
            //       // {
            //       //   'property': 'invoice_no',
            //       //   'displayName': '發票號碼',
            //       //   'type': 'String'
            //       // },
            //       // {
            //       //   'property': 'invoice_period',
            //       //   'displayName': '發票期別',
            //       //   'type': 'String'
            //       // },
            //       {
            //         'property': 'contact_firstname',
            //         'displayName': '聯絡人',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'contact_phone',
            //         'displayName': '聯絡人電話',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'date_billed',
            //         'displayName': '帳單出帳時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_start',
            //         'displayName': '帳單計費起日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_end',
            //         'displayName': '帳單計費迄日',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_cancelled',
            //         'displayName': '帳單取消時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_confirmed',
            //         'displayName': '帳單生效時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_requested',
            //         'displayName': '帳單已請款時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_paid_partial',
            //         'displayName': '帳單已部分付款(尚未結清)時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'date_paid_full',
            //         'displayName': '帳單結清時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'bill_note',
            //         'displayName': '帳單備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'bill_note2',
            //         'displayName': '帳單第二備註',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'created_at',
            //         'displayName': '建立時間',
            //         'type': 'Date'
            //       },
            //       {
            //         'property': 'updated_at',
            //         'displayName': '更新時間',
            //         'type': 'Date'
            //       }
            //     ]
            //   },
            //   'exportSetup': {
            //     'rootId': 'edihubMgmt',
            //     'schema': [
            //       {
            //         'property': 'ees_name',
            //         'displayName': '轉出設定名稱',
            //         'type': 'String'
            //       },
            //       {
            //         'property': 'ees_code',
            //         'displayName': '轉出設定代碼',
            //         'type': 'String'
            //       }
            //     ]
            //   }
            // }
          }
        }
        commit('login_success', res)
        return res
      } else {
        commit('login_error', userRes)
        return userRes
      }
    }).catch((error) => {
      console.log(error)
      // console.log(error.response)
      commit('login_error', error.response)
      return error.response
    })
  },
  register ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.Auth.register}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('register', response.data)
      return response.data
    }).catch(() => {
      // console.log(error.response)
    })
  },
  sendRestPWD ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.Auth.resetPWD}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('sendRestPWD', response.data)
      return response.data
    }).catch(() => {
      // console.log(error.response)
    })
  },
  verifyEmail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.Auth.verifyEmail}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      commit('verifyEmail', response.data)
      return response.data
    }).catch(() => {
      // console.log(error.response)
    })
  },
  resendVerifyEmail ({ commit, state }, data) {
    // console.log(data)
    // 重發驗證信
    let api = `${apiBaseUrl}${Config.API.Auth.resendVerifyEmail}`
    return app.axios.get(api).then((response) => {
      // console.log(response.data)
      let data = response.data
      commit('resendVerifyEmail', data)
      return data
    }).catch((error) => {
      return error.response.data
    })
  },
  updateMenuState ({ commit, state }, data) {
    commit('updateMenuState', data)
  },
  async update ({ commit, state }, data) {
    // console.log(data)
    try {
      let userUiRes = await app.axios.put(`${apiBaseUrl}${Config.API.userUi.update}${data.uiId}`, data.userUiPostData)
      await app.axios.put(`${apiBaseUrl}${Config.API.user.update}${data.userId}`, data.userPostData)
      // let userRes = await app.axios.put(`${apiBaseUrl}${Config.API.user.update}${data.userId}`, data.userPostData)
      // console.log(userUiRes)
      commit('update', data)
      return userUiRes
    } catch (error) {
      // console.log(error)
    }
  },
  setWinGridActive ({ commit, state }, data) {
    // console.log(data)
    commit('setWinGridActive', data)
  },
  updateWarehouse ({ commit, state }, data) {
    // console.log(data)
    commit('updateWarehouse', data)
  },
  updateThemeColor ({ commit, state }, data) {
    // console.log(data)
    commit('updateThemeColor', data)
  },
  setAccessToken ({ commit, state }, data) {
    // console.log('setAccessToken:', data)
    commit('setAccessToken', data)
  },
  setRefreshToken ({ commit, state }, data) {
    // console.log('setRefreshToken:', data)
    commit('setRefreshToken', data)
  },
  onlineuserQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.Auth.onlineuserQuery}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('onlineuserQuery', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      commit('onlineuserQuery', 'error')
      return error.response.data
    })
  },
  onlineuserLogout ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.Auth.onlineuserLogout}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('onlineuserLogout', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  setAccessToken (state, data) {
    state.accessToken = data
    app.axios.defaults.headers.common['Authorization'] = `Bearer ${data}`
    app.$localStorage.set('accessToken', `Bearer ${data}`)
    // console.log('commit - AccessToken:', data)
    // console.log('commit - localStorage AccessToken:', app.$localStorage.get('accessToken'))
  },
  setRefreshToken (state, data) {
    state.refreshToken = data
    app.$localStorage.set('refreshToken', `${data}`)
    // console.log('commit - localStorage RefreshToken:', app.$localStorage.get('refreshToken'))
  },
  clear (state) {
    state.auth = null
    state.userInfo = null
    state.menuTree = null
    state.menuState = {}
    state.permissions = null
    state.functionSchema = null
    state.winGridActive = 0
    state.stageType = 'W'
    app.$localStorage.clear()
  },
  logout (state, data) {
    // console.log(data)
    state.auth = null
    state.userInfo = null
    state.menuTree = null
    state.menuState = {}
    state.permissions = null
    state.functionSchema = null
    state.winGridActive = 0
    state.stageType = 'W'
    app.axios.defaults.headers.common['Authorization'] = 'Basic d21zLWNsaWVudDpWWEtEREpic1JEZ1JzNzVOZnBmUzczTkRGSnJaRUxTcw=='
    app.$localStorage.clear()
  },
  login_success (state, data) {
    // console.log(data)
    //
    state.stageType = data.data.stageType
    app.$localStorage.set('stageType', data.data.stageType)
    //
    app.$localStorage.set('pinToggle', false)
    //
    state.auth = data
    //
    state.accessToken = data.data.accessToken
    state.refreshToken = data.data.refreshToken
    //
    state.userInfo = data.data.userInfo
    app.$localStorage.set('userInfo', data.data.userInfo)
    //
    state.permissions = data.data.permissions
    app.$localStorage.set('permissions', data.data.permissions)
    //
    state.functionSchema = data.data.functionSchema
    app.$localStorage.set('functionSchema', data.data.functionSchema)
    //
    let menuTree = data.data.menuTree
    state.menuTree = menuTree
    app.$localStorage.set('menuTree', menuTree)
    //
    let menuState = {}
    menuState['MainMgmt'] = []
    state.menuState = menuState
    app.$localStorage.set('menuState', menuState)
    // 動態配路由
    let router = [{
      path: '',
      component: () => import('@/views/Main'),
      // 設定此路由(包括children路由)的meta值
      // 可以在全域router.beforeEach 中篩選識別所有流過的路由
      meta: { requiresAuth: true },
      children: []
    }]
    let routerArray = []
    for (let i = 0, num = menuTree.length; i < num; i++) {
      let _d = {
        path: `/Main/${menuTree[i].id}`,
        name: `${menuTree[i].id}`,
        component: () => import('@/views/MainContainer')
      }
      routerArray.push(_d)
    }
    // console.log(routerArray)
    router[0].children = routerArray
    app.$localStorage.set('dynamicRoutes', router[0])
    app.$router.addRoutes(router)
  },
  login_error (state, data) {
    // console.log(data)
    // state.auth = data
    state.auth = null
    state.userInfo = null
    state.menuTree = null
    state.menuState = null
    state.permissions = null
    state.functionSchema = null
    state.accessToken = null
    state.refreshToken = null
    app.axios.defaults.headers.common['Authorization'] = 'Basic d21zLWNsaWVudDpWWEtEREpic1JEZ1JzNzVOZnBmUzczTkRGSnJaRUxTcw=='
    app.$localStorage.clear()
  },
  register (state, data) {
    // console.log(data)
  },
  verifyEmail (state, d) {
    // 註冊驗證
    // console.log(d)
  },
  resendVerifyEmail (state, d) {
    // 重發註冊驗證信
    // console.log(d)
  },
  sendRestPWD (state, data) {
    // console.log(data)
  },
  updateMenuState (state, data) {
    // console.log(data.type)
    // console.log(data)
    switch (data.type) {
      case 'change':
        state.menuState[data.rf] = [data.w]
        break
      case 'add':
        state.menuState[data.rf] = [data.w]
        // if (!state.menuState[data.rf].includes(data.w)) {
        //   state.menuState[data.rf].push(data.w)
        // }
        break
      case 'remove':
        let _list = state.menuState[data.rf].filter((ele, idx) => {
          // console.log(ele)
          // console.log(data.w)
          return ele !== data.w
        })
        state.menuState[data.rf] = _list
        // console.log(state.menuState[data.rf])
        break
      case 'removeAll':
        state.menuState[data.rf] = []
        break
    }
    app.$localStorage.set('menuState', state.menuState)
    state.menuState = JSON.parse(JSON.stringify(state.menuState))
  },
  update (state, data) {
    // console.log(data)
    state.userInfo.layoutMode = data.userUiPostData.layout_mode
    state.userInfo.themeColor = data.userUiPostData.theme_color
    state.userInfo.avatar = data.userUiPostData.avatar
    state.userInfo.userName = data.userPostData.user_name
    // 放進localStorage
    app.$localStorage.set('userInfo', state.userInfo)
    state.userInfo = JSON.parse(JSON.stringify(state.userInfo))
    // console.log(localStorage)
  },
  setWinGridActive (state, data) {
    state.winGridActive = data
  },
  updateWarehouse (state, data) {
    // console.log(data)
    state.userInfo.warehousLocalName = data.wh_name
    state.userInfo.warehousEnglishName = data.wh_name_eng
    state.userInfo.warehouseKey = data.wh_id
    app.$localStorage.set('userInfo', state.userInfo)
    state.userInfo = JSON.parse(JSON.stringify(state.userInfo))
  },
  updateThemeColor (state, data) {
    // console.log(data)
    state.userInfo.themeColor = data
    app.$localStorage.set('userInfo', state.userInfo)
    state.userInfo = JSON.parse(JSON.stringify(state.userInfo))
  },
  onlineuserQuery (state, data) {
    // console.log(data)
    if (data === 'error') {
      state.auth = null
      state.userInfo = null
      state.menuTree = null
      state.menuState = {}
      state.permissions = null
      app.axios.defaults.headers.common['Authorization'] = 'Basic d21zLWNsaWVudDpWWEtEREpic1JEZ1JzNzVOZnBmUzczTkRGSnJaRUxTcw=='
      app.$localStorage.clear()
    }
  },
  onlineuserLogout (state, data) {
    //
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
