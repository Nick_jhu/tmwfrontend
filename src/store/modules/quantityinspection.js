import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  quantityinspection ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.quantityinspection.quantityinspection}${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('quantityinspection', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  quantityinspection (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
