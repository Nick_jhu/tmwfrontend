import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  productSnList: null,
  productSn: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearProductSnList ({ commit, state }, data) {
    commit('clearProductSnList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.productSn)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.productSn)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.productSn)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  createBulk ({ commit, state }, data) {
    console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.createBulk}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('create', response.data.respondCode.additionals)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.productSn.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  deleteBulk ({ commit, state }, data) {
    console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.deleteBulk}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('deleteBulk', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.dataexport}/${data.companyId}/${data.whId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  check ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.productSn.check}${data.companyId}/${data.whId}/${data.ownerId}/${data.productId}/${data.productSn}`
    return app.axios.post(api).then((response) => {
      // console.log(response.data)
      commit('check', response.data.respondCode.additionals.productSn)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.productSnList = null
    state.productSn = null
  },
  clearProductSnList (state) {
    state.productSnList = null
  },
  query (state, data) {
    state.productSnList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  deleteBulk (state, data) {
    // console.log('delete', data)
  },
  update (state, data) {
    state.productSnList = data
    // console.log(data)
  },
  dataexport (state) {
    // console.log(state)
  },
  check (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
