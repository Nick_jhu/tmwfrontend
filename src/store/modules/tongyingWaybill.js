import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
    tongyingWaybillList: null,
    tongyingWaybill: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
    clear({ commit, state }, data) {
        commit('clear')
    },
    cleartongyingWaybillList({ commit, state }, data) {
        commit('cleartongyingWaybillList')
    },
    clearDetail({ commit, state }, data) {
        commit('clearDetail')
    },
    create({ commit, state }, data) {
        // console.log(data)
        let api = `${apiBaseUrl}${Config.API.tongyingWaybill.create}`
        return app.axios.post(api, data, { timeout: 180000 }).then((response) => {
            // console.log(response.data)
            commit('create', response.data.respondCode.additionals.rec_shipping)
            return response.data
        }).catch((error) => {
            // console.log(error.response)
            return error.response
        })
    }
}

// mutations
const mutations = {
    clear(state) {
        state.tongyingWaybillList = null
        state.tongyingWaybill = null
    },
    cleartongyingWaybillList(state) {
        state.tongyingWaybillList = null
    },
    create(state, data) {
        // console.log(data)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
