import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  outboundQueryByCompanyWhList: null,
  outboundQueryByCompanyWh: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearOutboundQueryByCompanyWhList ({ commit, state }, data) {
    commit('clearOutboundQueryByCompanyWhList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundQueryByCompanyWh.query}${data.companyId}/${data.whId}/${data.ownerId}/${data.orderType}/${data.outboundStatus}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundQueryByCompanyWh.query}${data.companyId}/${data.whId}/${data.ownerId}/${data.orderType}/${data.outboundStatus}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundQueryByCompanyWh.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.outboundList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.outboundQueryByCompanyWhList = null
    state.outboundQueryByCompanyWh = null
  },
  clearOutboundQueryByCompanyWhList (state) {
    state.outboundQueryByCompanyWhList = null
  },
  query (state, data) {
    state.outboundQueryByCompanyWhList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.outboundQueryByCompanyWh = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.outboundQueryByCompanyWh = null
  },
  create (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
