import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  productList: null,
  product: null,
  productRelations: null,
  unitConverts: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearProductList ({ commit, state }, data) {
    commit('clearProductList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, id) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.queryDetail}${id}`
    return app.axios.get(api).then((response) => {
      console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.product.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  deleteProductRelations ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.product.deleteProductRelations}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('deleteProductRelations', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  deleteUnitConvert ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.product.deleteUnitConvert}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('deleteUnitConvert', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  import ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.product.import}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.productList = null
    state.product = null
  },
  clearProductList (state) {
    state.productList = null
  },
  query (state, data) {
    state.productList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.product = data.product
    state.productRelations = data.productRelations
    state.unitConverts = data.unitConverts
  },
  clearDetail (state, data) {
    state.product = null
    state.productRelations = null
    state.unitConverts = null
  },
  create (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  deleteProductRelations (state, data) {
    // console.log('delete', data)
  },
  deleteUnitConvert (state, data) {
    // console.log('delete', data)
  },
  update (state, data) {
    // console.log(data)
  },
  dataexport (state) {
    // console.log(state)
  },
  import (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
