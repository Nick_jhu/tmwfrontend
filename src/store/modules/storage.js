import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  storageList: null,
  storage: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearStorageList ({ commit, state }, data) {
    commit('clearStorageList')
  },
  clearStorage ({ commit, state }, data) {
    commit('clearStorage')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, id) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.queryDetail}${id}`
    return app.axios.get(api).then((response) => {
      console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryStorage ({ commit, state }, data) {
    // console.log(data)
    let sdId = null
    if (data.storage_id !== null && data.storage_id !== '') {
      // 儲位 以下
      sdId = data.storage_id
    }
    if (data.rack_detail_id !== null && data.rack_detail_id !== '') {
      // 貨架 以下
      sdId = data.rack_detail_id
    }
    if (data.level_detail_id !== null && data.level_detail_id !== '') {
      // 層 以下
      sdId = data.level_detail_id
    }
    if (data.level_detail_id !== null && data.level_detail_id !== '') {
      // 排/走道 以下
      sdId = data.level_detail_id
    }
    if (data.area1_detail_id !== null && data.area1_detail_id !== '') {
      // 儲區 以下
      sdId = data.area1_detail_id
    }
    if (data.floor_detail_id !== null && data.floor_detail_id !== '') {
      // 樓層 以下
      sdId = data.floor_detail_id
    }
    if (data.building_detail_id !== null && data.building_detail_id !== '') {
      // 棟/倉別 以下
      sdId = data.building_detail_id
    }
    //
    let api = `${apiBaseUrl}${Config.API.storageDetail.query}`
    let postData = {
      page: 1,
      pageSize: 1,
      combinedCondition: null,
      query: [
        {
          queryCondition: '=',
          queryField: 'sd_id',
          queryValue: sdId
        }
      ],
      sort: []
    }
    return app.axios.post(api, postData).then((response) => {
      // console.log(response.data)
      commit('queryStorage', { ...data, detailInfo: response.data.respondCode.additionals.storageDetail[0] })
      return {
        ...data,
        detailInfo: response.data.respondCode.additionals.storageDetail[0]
      }
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.update}${data.storageId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.httpStatus)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  queryEmpty ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.query}${data.whId}/empty/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryEmpty', response.data.respondCode.additionals.storage)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexportEmpty ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.dataexport}/${data.whId}/empty`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexportEmpty', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  import ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.storage.import}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.storageList = null
    state.storage = null
  },
  clearStorageList (state) {
    state.storageList = null
  },
  clearStorage (state) {
    state.storage = null
  },
  query (state, data) {
    state.storageList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.storage = data.data
  },
  queryStorage (state, data) {
    // console.log(data)
    state.storage = data
  },
  create (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  queryEmpty (state, data) {
    state.storageList = data
  },
  dataexport (state) {
    // console.log(state)
  },
  dataexportEmpty (state) {
    // console.log(state)
  },
  upload (state) {
    // console.log(state)
  },
  import (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
