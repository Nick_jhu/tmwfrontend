import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  withoutInProductRelationParentList: null,
  withoutInProductRelationParent: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearWithoutInProductRelationParentList ({ commit, state }, data) {
    commit('clearWithoutInProductRelationParentList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.withoutInProductRelationParent.query}${data.companyId}/owner/${data.ownerId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.withoutInProductRelationParent.query}${data.companyId}/owner/${data.ownerId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.withoutInProductRelationParent.query}${data.companyId}/owner/${data.ownerId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.product)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.withoutInProductRelationParentList = null
    state.withoutInProductRelationParent = null
  },
  clearWithoutInProductRelationParentList (state) {
    state.withoutInProductRelationParentList = null
  },
  query (state, data) {
    state.withoutInProductRelationParentList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.withoutInProductRelationParent = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.withoutInProductRelationParent = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
