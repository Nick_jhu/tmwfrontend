import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  outboundAdjustmentList: null,
  outboundAdjustment: null,
  orderAdjustmentDetailList: null,
  outboundAdjustmentDetailList: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearOutboundAdjustmentList ({ commit, state }, data) {
    commit('clearOutboundAdjustmentList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.queryDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  confirm ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.outboundAdjustment.confirm}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('confirm', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.outboundAdjustmentList = null
    state.outboundAdjustment = null
  },
  clearAutboundAdjustmentList (state) {
    state.outboundAdjustmentList = null
  },
  query (state, data) {
    state.outboundAdjustmentList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.outboundAdjustment = data.recOutbound || null
    state.orderAdjustmentDetailList = data.orderDetail || []
    state.outboundAdjustmentDetailList = data.outboundDetail || []
  },
  clearDetail (state, data) {
    state.outboundAdjustment = null
    state.orderAdjustmentDetailList = null
    state.outboundAdjustmentDetailList = null
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  confirm (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
