import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  product ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickinglist.product}/${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('product', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  order ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickinglist.order}/${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('order', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  consignee ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.pickinglist.consignee}/${data.userId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('consignee', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  product (state) {
    // console.log(state)
  },
  order (state) {
    // console.log(state)
  },
  consignee (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
