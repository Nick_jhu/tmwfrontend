import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  inboundDetailList: null,
  inboundDetail: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearInboundDetailList ({ commit, state }, data) {
    commit('clearInboundDetailList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryByCompanyWh ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.queryByCompanyWh}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('queryByCompanyWh', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  searchByCompanyWh ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.queryByCompanyWh}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('searchByCompanyWh', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  convert ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundDetail.convert}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('convert', response.data.respondCode.additionals.inboundDetail)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.inboundDetailList = null
    state.inboundDetail = null
  },
  clearInboundDetailList (state) {
    state.inboundDetailList = null
  },
  query (state, data) {
    state.inboundDetailList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryByCompanyWh (state, data) {
    state.inboundDetailList = data
  },
  searchByCompanyWh (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    state.inboundDetailList = data
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  convert (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
