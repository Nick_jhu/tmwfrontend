import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  billList: null,
  bill: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearBillList ({ commit, state }, data) {
    commit('clearBillList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.query}`
    return app.axios.post(api, data).then((response) => {
      console.log(response.data)
      commit('query', response.data.respondCode.additionals.bill)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.bill)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.respondCode.additionals.bill)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  autoDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.autoDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('autoDetail', response.data.respondCode.additionals.bill)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  cancele ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.cancele}${data.detailId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('cancele', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.bill)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  bulk ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.bulk}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('bulk', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  updateStatus ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.updateStatus}${data.detailId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('updateStatus', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.dataexport}/${data.companyId}/${data.whId}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  getBilledDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.bill.getBilledDetail}`
    return app.axios.post(api).then((response) => {
      // console.log(response.data)
      commit('getBilledDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.billList = null
    state.bill = null
  },
  clearBillList (state) {
    state.billList = null
  },
  query (state, data) {
    console.log(data)
    state.billList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  autoDetail (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.bill = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.bill = null
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  cancele (state, data) {
    // console.log('delete', data)
  },
  bulk (state, data) {
    // console.log(data)
  },
  updateStatus (state, data) {
    // console.log(data)
  },
  dataexport (state) {
    // console.log(state)
  },
  getBilledDetail (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
