import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  boxDetailsQueryList: null,
  boxDetailsQuery: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearBoxDetailsQueryList ({ commit, state }, data) {
    commit('clearBoxDetailsQueryList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxDetailsQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.boxDetailList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxDetailsQuery.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.boxDetailList)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.boxDetailsQuery.dataexport}`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.boxDetailsQueryList = null
    state.boxDetailsQuery = null
  },
  clearBoxDetailsQueryList (state) {
    state.boxDetailsQueryList = null
  },
  query (state, data) {
    state.boxDetailsQueryList = data
  },
  search (state, data) {
    // console.log(data)
  },
  dataexport (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
