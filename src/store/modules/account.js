import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  accountList: null,
  account: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearAccountList ({ commit, state }, data) {
    commit('clearAccountList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.update}${data.accountDetailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.account)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.account)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data.respondCode.additionals.account)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.account)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  delete ({ commit, state }, id) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.account.delete}${id}`
    return app.axios.delete(api).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  nonce ({ commit, state }, data) {
    console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.nonce}`
    return app.axios.post(api, data).then((response) => {
      console.log(response)
      // console.log(response.data)
      commit('nonce', response.data.respondCode.additionals.nonce)
      return response.data
    }).catch((error) => {
      console.log(error.response)
      return error.response
    })
  },
  login ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.account.login}`
    return app.axios.post(api, data).then((response) => {
      commit('login', response.data.respondCode.additionals)
      return response.data
    }).catch((error) => {
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.accountList = null
    state.account = null
  },
  clearAccountList (state) {
    state.accountList = null
  },
  query (state, data) {
    state.accountList = data
  },
  create (state, data) {
    // console.log(data)
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.account = data.length === 1 ? data[0] : null
  },
  clearDetail (state, data) {
    state.account = null
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  nonce (state, data) {
    // console.log('nonce', data)
  },
  login (state, data) {
    // console.log('login', data)
  },
  update (state, data) {
    // console.log('update', data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
