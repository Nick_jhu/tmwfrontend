import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  printwaybillList: null,
  printwaybill: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPrintwaybillList ({ commit, state }, data) {
    commit('clearPrintwaybillList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  hct ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.printwaybill.hct}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('hct', response.data.respondCode.additionals.way_bill_hct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  kerrytj ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.printwaybill.kerrytj}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('kerrytj', response.data.respondCode.additionals.way_bill_hct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  pelican ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.printwaybill.pelican}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('pelican', response.data.respondCode.additionals.way_bill_hct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  tongying ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.printwaybill.tongying}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('tongying', response.data.respondCode.additionals.way_bill_hct)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.printwaybillList = null
    state.printwaybill = null
  },
  clearPrintwaybillList (state) {
    state.printwaybillList = null
  },
  hct (state, data) {
    // console.log(data)
  },
  kerrytj (state, data) {
    // console.log(data)
  },
  pelican (state) {
    // console.log(state)
  },
  tongying (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
