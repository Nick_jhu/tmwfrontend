import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  exportSetupList: null,
  exportSetup: null
}

const apiBaseUrl = `${Config.API.EDIHubURL}:${Config.API.EDIHubURLPort}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearExportSetupList ({ commit, state }, data) {
    commit('clearExportSetupList')
  },
  xmlQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.xmlQuery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('xmlQuery', response.data.respondCode.additionals.xmlExportSetting)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  xmlExport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.xmlExport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('xmlExport', response.data.respondCode.additionals.DataExportResponse)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  excelQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.excelQuery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('excelQuery', response.data.respondCode.additionals.ExcelExportSetting)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  excelExport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.excelExport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('excelExport', response.data.respondCode.additionals.excelDataExport)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  csvQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.csvQuery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('csvQuery', response.data.respondCode.additionals.csvExportSetting)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  csvExport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.csvExport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('csvExport', response.data.respondCode.additionals.dataExportResponse)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  jsonQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.jsonQuery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('jsonQuery', response.data.respondCode.additionals['json-export-setting'])
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  jsonExport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.exportSetup.jsonExport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('jsonExport', response.data.respondCode.additionals.dataExportResponse)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.exportSetupList = null
    state.exportSetup = null
  },
  clearExportSetupList (state) {
    state.exportSetupList = null
  },
  xmlQuery (state, data) {
    // console.log(data)
  },
  xmlExport (state, data) {
    // console.log(data)
  },
  excelQuery (state, data) {
    // console.log(data)
  },
  excelExport (state, data) {
    // console.log(data)
  },
  csvQuery (state, data) {
    // console.log(data)
  },
  csvExport (state, data) {
    // console.log(data)
  },
  jsonQuery (state, data) {
    // console.log(data)
  },
  jsonExport (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
