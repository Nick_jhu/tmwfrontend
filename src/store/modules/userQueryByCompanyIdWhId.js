import {
  app
} from '../../main'
import Config from '../../Config'

// initial state
const state = {
  userQueryByCompanyIdWhIdList: null,
  userQueryByCompanyIdWhId: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearUserQueryByCompanyIdWhIdList ({ commit, state }, data) {
    commit('clearUserQueryByCompanyIdWhIdList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.userQueryByCompanyIdWhId.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('query', response.data.respondCode.additionals.user)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.userQueryByCompanyIdWhId.query}${data.companyId}/${data.whId}/q`
    return app.axios.post(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('search', response.data.respondCode.additionals.user)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.userQueryByCompanyIdWhIdList = null
    state.userQueryByCompanyIdWhId = null
  },
  clearUserQueryByCompanyIdWhIdList (state) {
    state.userQueryByCompanyIdWhIdList = null
  },
  query (state, data) {
    state.userQueryByCompanyIdWhIdList = data
    // console.log(state)
  },
  search (state, data) {
    // console.log(data)
  },
  clearDetail (state, data) {
    state.userQueryByCompanyIdWhId = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
