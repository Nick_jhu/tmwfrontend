import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  inboundAdjustmentList: null,
  inboundAdjustment: null,
  inboundAdjustmentDetailList: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearInboundAdjustmentList ({ commit, state }, data) {
    commit('clearInboundAdjustmentList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundAdjustment.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundAdjustment.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundAdjustment.queryDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundAdjustment.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error)
      return error.response.data
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.inboundAdjustment.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.inboundAdjustment.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.inboundAdjustmentList = null
    state.inboundAdjustment = null
  },
  clearInboundAdjustmentList (state) {
    state.inboundAdjustmentList = null
  },
  query (state, data) {
    state.inboundAdjustmentList = data
  },
  search (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    state.inboundAdjustment = data.recInbound || null
    state.inboundAdjustmentDetailList = data.inboundDetail || []
  },
  clearDetail (state, data) {
    state.inboundAdjustment = null
    state.inboundAdjustmentDetailList = null
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  update (state, data) {
    // console.log(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
