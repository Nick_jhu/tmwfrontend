import { app } from '../../main'
import Config from '../../Config'

// initial state
const state = {
  putInOperationSimpleList: null,
  putInOperationSimple: null,
  putInOperationSimpleDetailList: null,
  inventoryQuery: null,
  putawayInboundDetails: null
}

const apiBaseUrl = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}`
// console.log(apiBaseUrl)

// getters
const getters = {}

// actions
const actions = {
  clear ({ commit, state }, data) {
    commit('clear')
  },
  clearPutInOperationSimpleList ({ commit, state }, data) {
    commit('clearPutInOperationSimpleList')
  },
  clearDetail ({ commit, state }, data) {
    commit('clearDetail')
  },
  query ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  returnQuery ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.returnQuery}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('query', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  search ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.query}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('search', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  queryDetail ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.queryDetail}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('queryDetail', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  detailSearch ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.detailQuery}`
    return app.axios.post(api, data).then((response) => {
      commit('detailSearch', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  stockDetailsSearch ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.detailQuery}`
    return app.axios.post(api, data).then((response) => {
      commit('stockDetailsSearch', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  create ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.create}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('create', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  update ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.update}${data.detailId}`
    return app.axios.put(api, data.postData).then((response) => {
      // console.log(response.data)
      commit('update', response.data.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response.data
    })
  },
  delete ({ commit, state }, data) {
    // console.log(id)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.delete}`
    return app.axios.delete(api, { data: data }).then((response) => {
      // console.log(response.data)
      commit('delete', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.dataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  returnDataexport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.returnDataexport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  dataexportSnapshot ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.dataexportSnapshot}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('dataexportSnapshot', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  import ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.import}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('import', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  productsnImport ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.productsnImport}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('productsnImport', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  inboundCountStatus ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.inboundCountStatus}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('inboundCountStatus', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  inboundReturnCountStatus ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.inboundReturnCountStatus}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('inboundCountStatus', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  accept ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.accept}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('accept', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  qualityInspection ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.qualityInspection}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('qualityInspection', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  putaway ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.putaway}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('putaway', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  backAccept ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.backAccept}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('backAccept', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  },
  backQualityInspection ({ commit, state }, data) {
    // console.log(data)
    let api = `${apiBaseUrl}${Config.API.putInOperationSimple.backQualityInspection}`
    return app.axios.post(api, data).then((response) => {
      // console.log(response.data)
      commit('backQualityInspection', response.data)
      return response.data
    }).catch((error) => {
      // console.log(error.response)
      return error.response
    })
  }
}

// mutations
const mutations = {
  clear (state) {
    state.putInOperationSimpleList = null
    state.putInOperationSimple = null
    state.inventoryQuery = null
    state.putInOperationSimpleDetailList = null
  },
  clearPutInOperationSimpleList (state) {
    state.putInOperationSimpleList = null
  },
  query (state, data) {
    // console.log(data)
    state.putInOperationSimpleList = data
  },
  returnQuery (state, data) {
    // console.log(data)
    state.putInOperationSimpleList = data
  },
  search (state, data) {
    // console.log(data)
  },
  queryDetail (state, data) {
    // console.log(data)
    state.putInOperationSimple = data.recInbound || null
    state.putInOperationSimpleDetailList = data.inboundDetail || []
    state.putawayInboundDetails = data.putawayInboundDetails || []
  },
  clearDetail (state, data) {
    state.putInOperationSimple = null
    state.putInOperationSimpleDetailList = null
    state.putawayInboundDetails = null
  },
  detailSearch (state, data) {
    state.inventoryQuery = data
  },
  stockDetailsSearch (state, data) {
    // console.log(data)
  },
  create (state, data) {
    // console.log(data)
  },
  update (state, data) {
    // console.log(data)
  },
  delete (state, data) {
    // console.log('delete', data)
  },
  dataexport (state) {
    // console.log(state)
  },
  returnDataexport (state) {
    // console.log(state)
  },
  dataexportSnapshot (state) {
    // console.log(state)
  },
  import (state) {
    // console.log(state)
  },
  productsnImport (state) {
    // console.log(state)
  },
  inboundCountStatus (state) {
    // console.log(state)
  },
  inboundReturnCountStatus (state) {
    // console.log(state)
  },
  accept (state) {
    // console.log(state)
  },
  qualityInspection (state) {
    // console.log(state)
  },
  putaway (state) {
    // console.log(state)
  },
  backAccept (state) {
    // console.log(state)
  },
  backQualityInspection (state) {
    // console.log(state)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
