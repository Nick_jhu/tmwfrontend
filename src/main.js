import Vue from 'vue'
import Storage from 'vue-web-storage'
import axios from 'axios'
import createAuthRefreshInterceptor from 'axios-auth-refresh'
import VueAxios from 'vue-axios'
import iView from 'view-design'

//
import Config from './Config'
import App from './App.vue'
import router from './router'
import store from './store/index'
import i18n from './i18n'
import 'view-design/dist/styles/iview.css' // iView CSS
import 'vue-search-select/dist/VueSearchSelect.css'
//
import enUs from 'view-design/dist/locale/en-US'

// Font Awesome 5 : 需要再引入該icon
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronLeft, faSave, faCoffee, faChevronRight, faCaretRight, faDollarSign, faCaretUp, faCaretDown, faFile, faUser, faEllipsisH, faStar, faPlus, faClone, faBars, faKiwiBird, faLock, faLockOpen, faBell, faMinus, faTimes, faExpand, faCompress, faTachometerAlt, faBoxOpen, faWarehouse, faTrashAlt, faSearch, faCheck, faTrash, faPen, faSync, faSignInAlt, faPrint, faCloudUploadAlt, faCloudDownloadAlt, faTruck, faChartLine, faPeopleCarry, faAngleRight, faListUl, faFileDownload, faBorderAll, faBoxes, faBox, faChevronCircleLeft, faChevronCircleRight, faFileImport, faEnvelope, faQuestion, faPhoneAlt, faFileExport, faFileInvoiceDollar, faHome, faReply, faWindowRestore, faDoorOpen, faMoon, faSun, faFileExcel, faRedoAlt, faClipboardCheck, faTruckLoading, faUserCircle, faKey, faArrowRight, faArrowLeft, faShippingFast, faCheckCircle, faClipboardList } from '@fortawesome/free-solid-svg-icons'
import { faGooglePlus } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import formCreate from '@form-create/iview'
// import { maker } from '@form-create/iview'

library.add(faChevronLeft, faSave, faCoffee, faGooglePlus, faChevronRight, faCaretRight, faDollarSign, faCaretUp, faCaretDown, faFile, faUser, faEllipsisH, faStar, faPlus, faClone, faBars, faKiwiBird, faLock, faLockOpen, faBell, faMinus, faTimes, faExpand, faCompress, faTachometerAlt, faBoxOpen, faWarehouse, faTrashAlt, faSearch, faCheck, faTrash, faPen, faSync, faSignInAlt, faPrint, faCloudUploadAlt, faCloudDownloadAlt, faTruck, faChartLine, faPeopleCarry, faAngleRight, faListUl, faFileDownload, faBorderAll, faBoxes, faBox, faChevronCircleLeft, faChevronCircleRight, faFileImport, faEnvelope, faQuestion, faPhoneAlt, faFileExport, faFileInvoiceDollar, faHome, faReply, faWindowRestore, faDoorOpen, faMoon, faSun, faFileExcel, faRedoAlt, faClipboardCheck, faTruckLoading, faUserCircle, faKey, faArrowRight, faArrowLeft, faShippingFast, faCheckCircle, faClipboardList)
Vue.component('font-awesome-icon', FontAwesomeIcon)
// vue-fontawesome end

//
Vue.use(Storage)
Vue.use(VueAxios, axios)
Vue.use(iView, { locale: enUs }) // iView 預設語系 : en-US
Vue.use(formCreate)
Vue.use(require('vue-moment'))

//
Vue.config.productionTip = false
// superUser
// var superUserList = []
// let api = `${Config.API.localAPIURL}${Config.API.localAPIURLPort}${Config.API.localRootPath}${Config.API.Auth.maxLoginUser}`
// axios.get(api).then((r) => {
//   superUserList = r.data.super_user
// })
// Cache-Control
// Vue.axios.defaults.headers.common['Cache-Control'] = 'no-cache'
// Content-Type
Vue.axios.defaults.headers.common['Content-Type'] = `application/json;charset=UTF-8`
// Authorization
if (localStorage.getItem('app_accessToken')) {
  let re = /["]/g
  let appAccessToken = localStorage.getItem('app_accessToken')
  let accessToken = appAccessToken.replace(re, '')
  Vue.axios.defaults.headers.common['Authorization'] = accessToken
} else {
  Vue.axios.defaults.headers.common['Authorization'] = `Basic d21zLWNsaWVudDpWWEtEREpic1JEZ1JzNzVOZnBmUzczTkRGSnJaRUxTcw==`
}
//
Vue.axios.defaults.headers.common['Accept'] = `application/json;charset=UTF-8`
// Vue.axios.defaults.headers.common['Access-Control-Allow-Origin'] = `*`

const refreshAuthLogic = (failedRequest) => {
  // // console.log('failedRequest.response :', failedRequest.response)
  // // console.log('failedRequest.response :', failedRequest.response.config.url)
  // // console.log('failedRequest.response :', failedRequest.response.config.method)
  // // console.log('failedRequest.response :', failedRequest.response.config.data)
  // let re = /["]/g
  // let appNickname = localStorage.getItem('app_nickname') || ''
  // let nickname = appNickname.replace(re, '')
  // let superUser = superUserList.find((item) => {
  //   return item === nickname
  // })
  // // console.log(nickname)
  // // console.log(superUser)
  // if (nickname === superUser) {
  //   Vue.axios.defaults.headers.common['Authorization'] = `Basic dGVzdC1jbGllbnQ6VlhLRERKYnNSRGdSczc1TmZwZlM3M05ERkpyWkVMU3M=`
  // } else {
  //   Vue.axios.defaults.headers.common['Authorization'] = `Basic d21zLWNsaWVudDpWWEtEREpic1JEZ1JzNzVOZnBmUzczTkRGSnJaRUxTcw==`
  // }
  // //
  // let uid = store.state.auth.userInfo.uid || null
  // let refreshToken = store.state.auth.refreshToken || app.$localStorage.get('refreshToken')
  Vue.axios.defaults.headers.common['Refreshtoken'] = store.state.auth.refreshToken || app.$localStorage.get('refreshToken')
  let api = `${Config.API.baseURL}:${Config.API.port}${Config.API.rootPath}${Config.API.Auth.refreshtoken}`
  let postData = {
    client: 'web_client'
  }
  return axios.post(api, postData).then(response => {
    // return axios.post(api, postData, { skipAuthRefresh: true }).then(response => {
    console.log('response :', response)
    delete axios.defaults.headers.common['Refreshtoken']
    // console.log('response.data :', response.data)
    // let res = response.data
    // let accessToken = res.respondCode.additionals.refreshTokenResponse.access_token
    // let refreshToken = res.respondCode.additionals.refreshTokenResponse.refresh_token
    // // console.log('accessToken :', accessToken)
    // // console.log('refreshToken :', refreshToken)
    // store.dispatch('auth/setAccessToken', accessToken)
    // store.dispatch('auth/setRefreshToken', refreshToken)
    //
    // failedRequest.response.config.headers['Authorization'] = `Bearer ${accessToken}`
    return Promise.resolve()
  }).catch((err) => {
    delete axios.defaults.headers.common['Refreshtoken']
    app.$localStorage.clear()
    router.replace({
      path: '/login'
    })
    return Promise.reject(err)
  })
  // app.$localStorage.clear()
  // router.replace({
  //   path: '/login'
  // })
  // let err = 'error'
  // return Promise.reject(err)
}

createAuthRefreshInterceptor(axios, refreshAuthLogic, {
  statusCodes: [ 401 ],
  skipWhileRefreshing: false
})

export const app = new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')

//
window['vue'] = App
//
window.isNumber = function (obj) {
  return obj === +obj
}
window.isString = function (obj) {
  return obj === obj + ''
}
window.isBoolean = function (obj) {
  return obj === !!obj
}
