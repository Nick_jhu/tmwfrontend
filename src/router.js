import Vue from 'vue'
import Router from 'vue-router'

//
const Main = () => import('@/views/Main') // lazy load
const P404 = () => import('@/views/P404') // lazy load
const Login = () => import('@/views/Login') // lazy load
const MainContainer = () => import('@/views/MainContainer') // lazy load
const Tracking = () => import('@/views/Tracking') // lazy load
const InventoryQuery = () => import('@/views/InventoryQuery') // lazy load
const PrintShippingPic = () => import('@/views/PrintShippingPic') // lazy load
const PrintTag = () => import('@/views/PrintTag') // lazy load
const PrintNutritionFacts = () => import('@/views/PrintNutritionFacts') // lazy load

Vue.use(Router)

let routes = [
  {
    path: '/',
    redirect: '/Login',
    component: Login
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path: '/Main',
    name: 'Main',
    component: Main,
    redirect: '/Main/home',
    meta: { requiresAuth: true },
    children: [
      {
        path: `home`,
        name: 'home',
        component: () => import('@/views/MainContainer')
      }
    ]
  },
  {
    path: '/Main',
    name: 'Main',
    component: Main,
    redirect: '/Main/dashboard',
    meta: { requiresAuth: true },
    children: [
      {
        path: `dashboard`,
        name: 'dashboard',
        component: () => import('@/views/MainContainer')
      }
    ]
  },
  {
    // 開發component 測試用的頁面
    path: '/Main',
    name: 'Main',
    component: Main,
    redirect: '/Main/testView',
    meta: { requiresAuth: true },
    children: [
      {
        path: `testView`,
        name: 'testView',
        component: () => import('@/views/MainContainer')
      }
    ]
  },
  {
    path: '*',
    name: '404',
    component: P404
  },
  {
    path: '/Tracking',
    name: 'Tracking',
    component: Tracking
  },
  {
    path: '/InventoryQuery',
    name: 'InventoryQuery',
    component: InventoryQuery
  },
  {
    path: '/PrintShippingPic',
    name: 'PrintShippingPic',
    component: PrintShippingPic
  },
  {
    path: '/PrintShippingPic/:outboundId/:type',
    name: 'PrintShippingPic_OutboundId',
    component: PrintShippingPic
  },
  {
    path: '/PrintTag',
    name: 'PrintTag',
    component: PrintTag
  },
  {
    path: '/PrintTag',
    name: 'PrintTag_TagDetail',
    component: PrintTag
  },
  {
    path: '/PrintNutritionFacts',
    name: 'PrintNutritionFacts',
    component: PrintNutritionFacts
  },
  {
    path: '/PrintNutritionFacts',
    name: 'PrintNutritionFacts_TagDetail',
    component: PrintNutritionFacts
  }
]

if (window.localStorage.getItem('app_dynamicRoutes')) {
  let _route = JSON.parse(window.localStorage.getItem('app_dynamicRoutes'))
  _route.component = Main
  for (let i = 0, num = _route.children.length; i < num; i++) {
    _route.children[i].component = MainContainer
  }
  routes.splice(3, 0, _route)
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes
})
