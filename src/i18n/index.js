import Vue from 'vue'
import VueI18n from 'vue-i18n'

import cn from './lang/cn.json'
import en from './lang/en.json'
import tw from './lang/tw.json'

Vue.use(VueI18n)
const locale = (window.localStorage.getItem('app_language')) ? (window.localStorage.getItem('app_language')).replace(/^"?(.+?)"?$/, '$1') : 'zh_TW'
// console.log(locale)
const messages = {
  zh_CN: cn,
  en_US: en,
  zh_TW: tw
}

const i18n = new VueI18n({
  /** 默认值 */
  locale,
  messages
})

export default i18n
