# wms

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Run your end-to-end tests
```
yarn run test:e2e
```
### iVue UI 组件库
```
參考文檔: https://www.iviewui.com/
```

### 加icon
```
@/main.js
參考文檔: https://github.com/FortAwesome/vue-fontawesome
fontawesome 5: https://fontawesome.com/
```

### grid-layout : 位移 + 縮放
```
參考文檔: https://github.com/jbaysolutions/vue-grid-layout
```

### layout model
```
H / V
```

### theme color
```
預設 dark / light
```

###註1:
```
設計稿文字尺寸換算
30px > 25px 
25px > 21px 
20px > 16px 
15px > 14px
```
###註2:
"combinedCondition": null, "AND", "OR" ; 
"queryCondition":  >, <, >=, <=, =, LIKE
```
